+++
title = 'Week 03: Project Management'
date = 2024-01-25T13:42:01+02:00
+++
*Documentation site creation and repository management* 

<!--more-->

***

This post will cover two parts:
1. How I created this website, and
2. How I use Git and GitLab to manage files in my repository.

***
## Part 1. How I created this website

I used **[Hugo](https://gohugo.io/)**, an open-source framework, to set up my documentation website. Hugo is a static site generator, which means that it generates a full static HTML website based on raw data and a set of templates. Essentially, it 
>  "automates the task of coding individual HTML pages and gets those pages ready to serve to users ahead of time." (Reference: <https://www.cloudflare.com/en-gb/learning/performance/static-site-generator/>)

As the HTML pages are pre-built, the website can load very quickly in users' browsers. This characteristic also fit the situation of a learning documentation site, which will contain quite some images and videos as the course proceeds.

Another thing I like about Hugo is that, I can create the site content simply by writing in Markdown syntax. As the weekly assignment requires detailed description and explanation of the process, I feel writing in Markdown is a much more sustainable approach than repeatedly tags and classes (e.g., `<p>...</p>`), which is much more error-prone.

I have some experience writing documentation onto a Hugo site previously in the course Physical Computing. Yet, I did not set up the site myself and I was writing on the Web IDE. This time I want to have more control on the site, so I installed Hugo locally. To do so, I followed the tutorial here (for macOS): <https://gohugo.io/installation/macos/>, my basic steps are like this:
- Install **[Homebrew](https://brew.sh/)** (package manager)
 - key in this in the macOS terminal: `/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"`
- Install Hugo - `brew install hugo`
- Install **[Git](https://git-scm.com/)**
- Install **[Go](https://go.dev/)** (version 1.20 or later)
- Install **[GCC](https://gcc.gnu.org/)** (C compiler)
- Update the PATH environment variable

The confusing part is at the last - I did not quite understand where my module path is. By reading the Go documentation (and double-checking with ChatGPT), I managed to understand this part and thus compiled and run a test project.

I'm using **[VSCode](https://code.visualstudio.com/)** to edit my codes.

[![Hugo post markdown](./images/hugo--markdown.jpg)](./images/hugo--markdown.jpg)
*>> How a hugo post written in markdown look like in the text editor <<*

For now, I am still using **[Theme-Console](https://themes.gohugo.io/themes/hugo-theme-console/)** created by Marcin Mierzejewski for my site's appearance. I was then not sure about how a Hugo site folder structure would look like and how to create a theme from scratch, thus I decided to use an existing theme first and learn from it. After getting more familiar this framework, I later set up a learning diary site for another course and styled it by myself, it can be accessed here (if anyone is interested in *"Doing research in art and media"*): <https://l-lu-u.gitlab.io/art-media-research-diary-2024/>

### Work around Hugo
During this process, I have learnt a few "tricks"(?) to work with Hugo.

Firstly, I want to customise the website appeareance that is currently defined by the CSS from the theme. I found a tutorial on this topic here: [Hugo Mini Course: Custom CSS](https://hugo-mini-course.netlify.app/sections/styling/custom/). Thus, I created my own stylesheet `style.css` and placed it into `static` folder.

[![Raster image](./images/hugo--custom-css.jpg)](./images/hugo--custom-css.jpg)
*>> How I added my custom stylesheet <<*

Here is the html code I inserted in the header section (can be found in **"layouts -> _default -> baseof.html"**): 
```html
    <link href="{{ .Site.BaseURL }}css/style.css" rel="stylesheet">
```

Another challenge in using Hugo is actually to insert videos into the content. A solution is to create a shortcode that allows us to insert raw html mark up in the markdown posts - by default, Hugo will omit the raw html. Here are the two sources I references:
- Insert raw html <https://stackoverflow.com/questions/63198652/hugo-shortcode-ignored-saying-raw-html-omitted>
- Create Own Shortcode to add videos to hugo post <https://iamsorush.com/posts/add-video-to-hugo-post/>

[![Insert video](./images/hugo--video.jpg)](./images/hugo--video.jpg)
*>> How I inserted video in a hugo post <<*

I also figured out how to set my custom favicon (using emoticon) for this site.

## Part 2. How I use Git and GitLab to manage files in my repository

In general, my documentation process goes like this:
- Screenshot / take picture (phone or camera, depends) >> add explanation (using Figma) if necessary >> resize the image (within 1920 x 1920 px frame)
- Image Compression
 - Preview (macOS)
 - ImageMagick: for building web app, for patch editing
- Video Compression
 - ffmpeg or HandBrake

Here are the some basic commands one may use for **Git**:
- add changed files to the **staging area**: `git add .` ('.' as all changed files from the current directory)
- display the status of the working directory: `git status` (good to check what are actually staged for the commit)
- create a new commit from the staged changes: `git commit -m "..."` ('...' is your must-have message for each commit)
- push to the repo: `git push -u origin main`

Here is a git command cheat sheet I sometimes refer to: <https://about.gitlab.com/images/press/git-cheat-sheet.pdf>
Some more resources to learn git can be found here: <https://docs.gitlab.com/ee/topics/git/>

After pushing the commit, you may check its status under **"Build -> Pipelines"**, when the status turning from **"Running"** to **"Passed"**, you can see the change(s) on your website.

There are a few more other things I did for managing the repository.

First, within the project, I went to **"Deploy -> Pages"**, and then unechecked the **"unique domain"** option, thus the domain of my site can simply be accessed through "https://'my user name'.gitlab.io/'project name'" -> <https://l-lu-u.gitlab.io/2024-digital-fabrication>.

[![Uncheck the unique domain](./images/git--uncheck-unique-domain.jpg)](./images/git--uncheck-unique-domain.jpg)
*>> Uncheck the unique domain option <<*

Second is to set up a SSH key for my account, which allows me to establish a secure connection between my computer and GitLab. I followed the instruction here to generate the ED25519 SSH key pair: <https://docs.gitlab.com/ee/user/ssh.html>.

In sum, visit my repository here: <https://gitlab.com/l-lu-u/2024-digital-fabrication>

### On the difference between GitLab & GitHub
References: <https://www.geeksforgeeks.org/difference-between-gitlab-and-github/>

### To-do for the website
So, this is how I set up and manage my website. However, there are still something I would like to modify from the current appearance, especially:
1. The differentiation of heading sizes (for readability), and
2. the image can be clicked to enlarge (without refreshing the page).

***

## Some notes from the global lecture
- most popular compression (maybe in ) H.264
    - H265 -> 4k monitors
    - when packaged into a file, on web, mp4 or webm