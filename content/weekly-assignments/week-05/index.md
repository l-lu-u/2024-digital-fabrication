+++
title = 'Week 05: Computer-Controlled Cutting'
date = 2024-02-08T13:42:01+02:00
+++
*Laser Cutter, Vinyl Cutter, and Parametric Press-fit Kit* 

<!--more-->

***

Below are my outcomes for this week:

[![Laser cutting outcome](./images/laser--outcome.jpg)](./images/laser--outcome.jpg)
*>> Laser cutting outcome: an experiment with cellular structure <<*

[![Vinyl cutting outcome](./images/vinyl--outcome.jpg)](./images/vinyl--outcome.jpg)
*>> Vinyl cutting heat transfer outcome: PANOLLAMA totebag - shout out to Vertti L., Burak T., and Jonna E.<<*

In addition, I made a little parametric project on **Cuttle.xyz**, which can help one to easily conduct **comb test** for new material and thus *design for press-fit parts*. The project can be found here: **[comb test made easy](https://cuttle.xyz/@lu_u/comb-test-made-easy-9knZ2wZeQKZM)**

{{< rawhtml >}} 
<video width=100% controls>
    <source src="https://l-lu-u.gitlab.io/2024-digital-fabrication/videos/week05--cuttle-demo.mp4" type="video/mp4">
    Your browser does not support the video tag.  
</video>
{{< /rawhtml >}}

This post consists of 3 main parts regarding my working process and reflections:
1. Group introduction session (with the documentation from [Graham](https://grahamfeatherstone.gitlab.io/digital-fabrication/)),
2. Laser cut press-fit construction kit,
    1. The "comb test made easy" tool,
3. Vinyl cut (heat transfer) pattern.
    1. I noticed that there is **no Wiki about using the heat press machine** on FabLab's documentation site.

***

## Part 1. Group assignment: Laser cutter introduction and safety training 
Here is the link to group assignment written by Graham: <https://grahamfeatherstone.gitlab.io/digital-fabrication/Projects/LaserTraining/Laser.html>, which documents the introduction session done by [Burak](https://burakturkoz.gitlab.io/digital-fabrication-by-burak/) on laser cutting with **[Epilog Fusion Pro 48 80W](https://www.epiloglaser.com/laser-machines/fusion-pro-laser-series/)**. In this session, we were also introduced to the concept of **kerf** and the practice of **comb test**.

The Wiki page on Aalto FabLab also documents the instruction thoroughly, you may refer here: <https://wiki.aalto.fi/display/AF/Laser+Cutting+with+Epilog+Fusion+Pro+48?showCommentArea=true>.

**Glossary:**
- **kerf**: the width of material that is removed by a cugtting process, which varies according to the specific material, the focal length of the laser, and some other factors. When offsetting the kerf, we should expand the outline by half of the kerf value.
- **comb test**: using two comb-like parts with differently sized teeth to find out what is a suitable opening width for press-fit parts to be inserted into each other, we are inspired by [Nadieh's weekly assignment](https://fabacademy.org/2021/labs/waag/students/nadieh-bremer/blog/week-3/) in Fab Academy 2021.

**!!!Huom/Attention!!!** 
There are a few things to be noted:
1. **Not to make the cut piece too small** (i.e., smaller than 20x20 mm) - it will probably drop through the dashboard of the laser cutter,
2. After the cutting finishes, **wait for a few moments before opening the cover** so that the fume within can be extracted clearly,
3. Remember to measure the material thickness from different sides - especially the cardboard, the surface is usually not consistent. 

During this introduction session, we measured and tested a few values of the material(i.e., cardboard), which will be used for the assignment later. Though it may still vary slightly for each sheet, I noted these down here as references for my design later:
**Parameters (to take away) from the training**
- **material thickness**: ~3.75mm (cardboard)
- **kerf**: ~0.28mm ((20 x 5 - 98.6) / 5)

**Recommended setting for laser cutting machine**
- **power**: 75~100%
- **speed**: 20~40%

***

## Part 2. Individual assignment A: A parametric press-fit construction kit
For this assignment, I wish to design a cellular structure that resembles capillary beds, which I may possibly incorporate it into my final project. 

[![Sketch of capillary beds](./images/sketch-capillary.jpg)](./images/sketch-capillary.jpg)
*>> A line drawing of capillary bed-ish structure <<*

The capilary bed refers to an interwoven network of small blood vessels that supplies an organ. I find this structure interesting as this organic network often appears to be polygonal. Hence, I decided to a press-fit structure that can be constructed by using polygonal units. Below is a very rough sketch of this idea:

[![Sketch of each modular unit](./images/sketch-module.jpg)](./images/sketch-modular.jpg)
*>> A sketch of polygonal unit with different number of sides: 3, 4, 6... <<*

Next, I moved on to convert these vague ideas onto a digital format. To be honest, I do not have much experience in designing 3D objects (that need to be fabricated in reality). Thus, I often find it challenging to "envision" a 3D strucutre - how different parts should be jointed and how I shall start working on it. After wandering and pondering in different CAD tools for a while, I decided to start with [Cuttle](https://cuttle.xyz/dashboard) first. Cuttle is a browser-based parametric CAD software while students can use the school email to create an account with the free educational plan.

The interface of Cuttle looks intuitive enough for a novice user (with graphic design experience). Moreover, communnity projects and tutorials offer sufficient references for me to explore the tool with my ideas.

To start with, I created a hexagonal shape and a triangular shape as two seperate **components**, which allows me to replicate and reuse these shapes to construct a more complicated design later.

[![How the shapes are made](./images/cutting--boolean-difference.jpg)](./images/cutting--boolean-difference.jpg)
*>> How the shapes are made from circles and boolean difference <<*

In this process, the two functions I repeatedly applied is **"Modify -> Rotational Repeat"** and **"Modify -> Boolean Difference"**. Similary, I designed the slit as another component. And then, I placed one slit element to one end of the polygonal shape and repeat the **Rotational Repeat + Boolean Difference** to create the cutting for press-fit.

[![How the shapes are made](./images/cutting--slit.jpg)](./images/cutting--slit.jpg)
*>> Create the slit using mirror repeat <<*

[![Parametric design in Cuttle](./images/cuttle-parametric.png)](./images/cuttle-parametric.png)
*>> Set up parameters in Cuttle <<*

After I finished design the outline, I applied **"Modify -> Expand (Offset)"** to offset the kerf value. As shown in the prompt, the offset distance is set to **the half of the kerf width** (0.14mm).

[![All the shapes created](./images/cutting--shapes.jpg)](./images/cutting--shapes.jpg)
*>> All the shapes I created in the first draft <<*

Yet, one of my difficulties is that I am not proficient in imagining 3D structure. In order to validate this design, I set off to cut these shapes out to understand how they would actually work together. Below is the test cut result on Thursday morning (February 15th, 2024):

[![Test cut on Thursday morning](./images/test01-result.jpg)](./images/test01-result.jpg)

The press-fit slot is much narrower than expected, even thought I designed the width of the slit based on the material thickness. After checking the design file, I noticed that my slit was applied **a scale factor** around 0.75, I set it back to 1, and now the slit looks wider on the design file. 

Another thing I noticed is that, the hollow space within the hexagon is big enough for the triangular shape. In the first trial, I place the two shapes side-by-side, which wasted a full circle from cutting the hexagonal element. After getting to know the concept of **"nesting"** - laying out cutting patterns to minimize raw material waste, I manually placed the triangular shape within the hollow hexagon when I cut it for the second time.

Another benefits of nesting is to reduce the cutting time. By rearranging how the design lays on the sheet material, they can share perimeters and thus save the time. I wanted to use softwares for it, for example, I found this open-source and free application: [Deepnest](https://deepnest.io/). Unfortunately, the application always crashed during the export on my computer. Yet, I will continue to find one that works for me (in case I need to cut anything next time. Here are some auto-nesting options I found:
- **Adobe Illustrator** neting plugin: <https://community.adobe.com/t5/illustrator-discussions/auto-nesting-objects/m-p/3659299>
- **Deepnest** (free and open source): <https://deepnest.io/>
- **SVGnest** (free, open source, and web-based): <https://svgnest.com/test/>
- **Fusion 360** has a Nesting & Fabrication Extension


With the test cut results, I found that the triangular units are more flexibal in forming the 3D structures, for example, they can form an almost pentagonal shape. Hence, I only keep the triangular unit in the final result.

## Part 2.1. "comb test made easy" on Cuttle.xyz
Unexpectedly, I made something (possibly) useful on Cuttle.xyz. 

During the session, we noticed that the cardboard was thicker than expected, yet the "comb teeth" were manually sized in Illustrator. This procedure seems tedious and fallible if we need to work with different materials. After exploring Cuttle.xyz, I was inspired to make a parametric comb-test tool to simplify this process. Here you can find the project: <https://cuttle.xyz/@lu_u/comb-test-made-easy-9knZ2wZeQKZM>.

Below you can see the screen of my "comb test made easy" project:

[![How to use my comb test project](./images/cuttle--comb-test.jpg)](./images/cuttle--comb-test.jpg)

On the left, you can find two components - one for **comb test**, while the other is for the **kerf value test**. On the right, you can find the fields to update the parameters for the project and the components. Two of the parameters are especially important for us in this case: 1). **"material_thickness"**, and 2). **kerf**.

Here are the steps to use this tool:
1. Measure the material you are using and update the **"material_thickness"** accordingly,
2. Find out the **kerf** - if you would like to use the file in my project, go to **"COMPONENTS -> kerf-test"**,
3. After measuring and calculating, update the **"kerf"** in the project accordingly,
4. Now, navigate to **"COMPONENTS -> comb-test"**, export the design and send it to the laser cutter. **NOTE: You will need two combs to test the fitting <3**

[![Note the material thickness](./images/cuttle--comb-test-expanded.jpg)](./images/cuttle--comb-test-expanded.jpg)
*>> Note the material thickness <<*

***

## Part 3. Individual assignment B: Vinyl cutting (a slightly goofy project)
The vinyl cutter I'm using is [Roland GX-24](https://www.rolanddga.com/support/products/cutting/camm-1-gx-24-24-vinyl-cutter), a detailed instruction can be found here: <https://wiki.aalto.fi/display/AF/Vinyl+Cutting+with+Roland+GX-24>

Started from my friend Vertti, taking panorama pictures has become an obsessive activity for us in Berlin (during the transmediale 2024). Through the lens of new media students, we appreciated panorama photography - its elongated proportion, the glitch it often creates, the motions or emotions it preserves within the glitche images, its relations to surveillance (and panopticon), and more.

A few of us found it has some potential to evolve into a more complete project. Thus, I am experimenting with a (possible) merch pattern about panorama: TA-DA, **PANOLLAMA**. The vision I had was a bit quirky - a distorted llama as if it moves when someone was taking a panorama picture for it. I found a royalty free llma picture online (photo credits ©Dmitry Burlakov) and applied **"Path > Trace Bitmap"** to it in Inkscape. The tutorial of tracing bitmaps in Inkscape can be found here: <https://inkscape.org/doc/tutorials/tracing/tutorial-tracing.html>

[![Trace bitmap in Inkscape](./images/vinyl--sketch-to-inkscape.jpg)](./images/vinyl--sketch-to-inkscape.jpg)
*>> From sketch to a vector image <<*

After that, I selected parts of the anchorpoints and started stretching it in an irregular manner. Also, the image should be **mirrored horrizontally** as I planned to print it onto a tote bag using heat transfer vinyl. The design should be exported as a close shap that is filled with colour.

[![Trace bitmap in Inkscape](./images/vinyl--for-cut.jpg)](./images/vinyl--for-cut.jpg)
*>> For the actual print, I only keep the bigger llama from its neck above to save my effort in removing the vinyl <<*

When cutting the pattern, you may start heating the heat press machine (*!!! I haven't found a wiki page on how to use this yet on Aalto FabLab's documentation cite*). You can find the suggested temperature and time on the material's information sheet. For the heat transfer vinyl I'm working with, I set the pressing to be at 160 degree for 20 seconds.

[![Heat Press](./images/vinyl--heat-press.jpg)](./images/vinyl--heat-press.jpg)
*>> The heat press machine <<*

Here are a step-by-step instruction on how to use the heat press machine:
1. Before placing the item that you want to press the vinyl cut design onto - it might be a tote bag, T-shirt, hoodie, or whatever - put a sheet of baking paper on the bottom plate,
2. Place the item (e.g., tote bag, T-shirt, hoodie, or whatever) and cover it with another sheet of baking paper, so that the glue from the vinyl won't stuck to the machine
    1. Try to make it lay flat as much as possible, so it will be heated evenly,
3. Close the heat press lid, **BE CAREFUL, IT IS ACTUALLY VERY HOT**,
4. Once the lid is completely closed, the countdown will start, the machine will start beeping when it is done,
5. Open the lid carefully (you may need to press it down first and you will feel the lid start to be released) and remove the item from the machine,
6. Wait for it to cool down first before you remove the baking paper and the transparent plastic sheet, as the glue will still be hot, if we remove the covering sheet now, the vinyl pattern might be peeled together.
7. When the item feels cool enough, remove the baking paper and the transparent plastic sheet,
8. **REMEMBER TO TURN OFF THE MACHINE <3**

That's it.

[![Tote bag](./images/vinyl--tote-bag.jpg)](./images/vinyl--tote-bag.jpg)
*>> The tote bag <<*

Some questions I want to figure out next time is *how to vinyl cut a multi-colour design.* I suppose the alignment part may work similarly to a multi-colour block printing process.

## Other things I noted from the global and local lectures
- Living hinge (that kind of bendable)
    - <https://amandaghassaei.com/>
    - <https://fabacademy.org/2024/labs/barcelona/students/riichiro-yamamoto/index.html>
- Blender: strange attractor
    - chaos theory
- Flexible circuit board
    - cut on the copper tape ssing the vinyl cutter for