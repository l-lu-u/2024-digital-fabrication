+++
title = 'Week 01: Introduction'
date = 2024-01-12T14:42:01+02:00
+++
*Diving into GitLab and CI/CD* 

<!--more-->

***

For this week, I am setting up the documentation site for my upcoming fabrication project(s). And here you are, welcome!

This site is built with Hugo, a static site generator written in Go. I found it suitable to write and edit my content for the documentation purpose. 

Steps:
- Install Hugo
- Install Git
- Install Go (version 1.20 or later)
- Install GCC (C compiler)
- Update the PATH environment variable

The confusing part is at the last - I did not quite understand where my module path is. By reading the Go documentation (and double-checking with ChatGPT), I managed to understand this part and thus compiled and run a test project.


Tools used:
- [Hugo](https://gohugo.io)
- [Theme-Console](https://themes.gohugo.io/themes/hugo-theme-console/)
- [Go](https://go.dev/doc/install)
- [Homebrew](https://brew.sh/)
- [VSCode](https://code.visualstudio.com/)

> ### Notes & Resources
> 
> On the difference between GitLab and GitHub: <https://www.geeksforgeeks.org/difference-between-gitlab-and-github/>
> 
> Markdown language syntax: <https://www.markdownguide.org/basic-syntax/#links>
>
> Nadieh's Fab Academy (2021): <https://fabacademy.org/2021/labs/waag/students/nadieh-bremer/>