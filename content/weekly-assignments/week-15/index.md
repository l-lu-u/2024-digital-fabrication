+++
title = 'Week 15: Interface & Application'
date = 2024-05-02T14:42:01+02:00
+++
*Interface & Application*

<!--more-->

***

## Group Introduction

2024.05.08
by Kris & Saskia

### Creative coding framework
- **openFramework**
	- C++
	- able to carry (more?) hardwares
	- (Kyle McDonald)
	- motivation: *processing seems too slow*
	- parts
		- ofApp.h: define stuff
		- ofMain.h: functions?
	- difference to processing
		- openGL
		- update() & draw()
			- draw() > based
			- update() > according to the data
- **Processing**
	- Java
	- easy learning curve, Arduino was inspired by Processing
	- two parts
		- setup
		- draw
	- Steps
		- include processing.serial library
		- define classes
		- define variables
	- Cast the serial signal to string
- **p5.js** 
	- Javascript
	- web based
	- designed for interface, user actions like click or hover, have been already supported
	- REST Serial API 
	- async functions
- **pureData**
	- sound
- **touchdesigner**
	- node-based
	- not as light weight, need a computer

### Same interface written differently


### Media Archaeology of Creative Coding Tools

- Presessor of OF & Processing
	- acu - Tom White (https://github.com/dribnet/acu)
		- digital archaeology: https://hcommons.org/members/tomwhite/
		- smilar naming conventions to OF
	- Design by numbers - John Maeda
- Processing - Ben Fry & # Casey Reas
	- a reaction to the flash scene & Adobe's control over
- OF
	- [Future Sketches](https://www.media.mit.edu/groups/future-sketches/overview/)
- p5.js
- touchdesigner

timeline > open source movement 
open source v.s. privately owned / for profit / commercial
- why open source tools are not that in favour (among media art students) now?

Golan Levin <https://www.flong.com/archive/index.html>

***