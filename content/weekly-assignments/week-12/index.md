+++
title = 'Week 12: Input Devices'
date = 2024-04-10T14:42:01+02:00
+++
*Sensing the world* 

<!--more-->

***

##

pulse oximeter

why pulse:
- Pulse (heart rate) -> blood flows through
- Hormonal contraception, both the pills and the intrauterine device (IUD) can increase the risk of blood clots, heart attack, and stroke (link: <https://www.cochrane.org/CD011054/FERTILREG_risk-heart-attack-and-stroke-women-using-birth-control-pills>)


***
## Global lecture

pulse sensing -> relevant for my final project
[MAX3010](https://www.analog.com/media/en/technical-documentation/data-sheets/MAX30100.pdf)



step response: 
<https://eng.libretexts.org/Bookshelves/Industrial_and_Systems_Engineering/Introduction_to_Control_Systems_(Iqbal)/02%3A_Transfer_Function_Models/2.04%3A_System_Response_to_Inputs>


I2C (i squre c), interface:
- SCL
- SDA

6DoF -> 6 degrees of freedom