+++
title = 'Week 11: Output Devices'
date = 2024-03-21T14:42:01+02:00
+++
*Add an output device (e.g., DC motor driver) to the microcontroller board (and program it to do something)* 

<!--more-->

***

## Week 11. Overview

***


Thermal relief

RPM sensor -> 

***

Motor:
- DC motor
- Servo motor
- **Stepper motor** -> more powerful, with more precise control for my final project



check about torque

FTDI (Future Technology Devices International), USB to Serial

***

## Programming

Based on the code written in [Week 08 - Embedded Programming](), in this version, I replaced the `delay()` function with `millis()` to manage the button status checking, cycle progression, and LED brightness changes without blocking other functions.

***

## Others
[Comparison of brushed DC motors vs. servo motors](https://www.thomasnet.com/articles/machinery-tools-supplies/dc-motors-vs-servo-motors/)