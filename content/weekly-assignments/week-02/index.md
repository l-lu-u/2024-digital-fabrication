+++
title = 'Week 02: Final Project Proposal(s)'
date = 2024-01-18T14:42:01+02:00
+++
*Some ideas to revise project SPILL* 

<!--more-->

***

For digital fabrication final, I plan to revise and polish the project *SPILL* (from course Physical Computing 2023 Autumn). Here is the link to [previous course documentation and reflection page](https://newmedia.dog/students/2023/lu-chen/physical-computing/final-project/).

*SPILL* is a kinetic installation that represents menstrual cycles mechanically with a set of tea wares found in a recycling centre. Through this work, I intend to depict some absurdity about this common (yet often taboo) biological process as well as to invite the audience to openly discuss about their exprience and understanding.

The initial sketch and the exhibited version in December 2023 are shown below:
[![Initial sketch and the exhibited prototype in 2023](./images/spill--version-1.jpg)](./images/spill--version-1.jpg)

And here is the measurement from this prototype, which can be used as a reference for the upcoming revision.
[![Prototype dimensions in 2023](./images/spill--prototype-dimension.jpg)](./images/spill--prototype-dimension.jpg)

Here are some of my goals (or wishes) for this iteration:
- **Form development:** 
- - Design the supporting frame to be more organic looking, the overall dimension should be able to fit on a tea table;
- - Design the clamping structures for motors and cups with a similar organic look and feel with the overall structure.
- **Interaction Design:**
- - Visualise and/or sonify the waiting time, which can be designed according to the menstrual cycle as well;
- - Use input like the audience’s pulse or breathing (28 counts maybe?) to control the "period duration" instead of the online dataset. 
- **Mechanism:**
- - I think it is possible to reduce at least one motor to control the movement for both ovaries, so that the installation can be more efficient.
- **Robustness:**
- - Revise the spool structure and the string length - for the current version, the tampon is controled with a string wounded on a spool, while the spool is attached to a stepper motor on top of the frame. However, the string is often entangled on the spool. Or, there can be an alternative way to dip and lift the tampon;
- - Review the usage of magnetometer. Currently, I use 0.65 for either x/y/z direction as the threshold to stop the spooling, I can either stablise the tampon (and the spoon), or investigate more about the ways of working of magnetometer;
- - A self-calibration process to be;
- - More secured wiring.
- **Experimentation:**
- - Soft robotics;
- - 3D printing -> mold casting -> ceramics + glass;
- - 3D clay printing 

A visual moodboard for now (January, 2024):
[![Visual Moodboard](./images/spill--visual-moodboard.jpg)](./images/spill--visual-moodboardt.jpg)

An inspiration is Kelly Dobson's ScreamBody done at the "How to Make (Almost) Anything," link to the documentation: <https://web.media.mit.edu/~monster/screambody/>. The concept of "Wearable Body Organs" inspired me to contemplate more the design of my mechanical menstrual kit.

Link to Kelly Dobson's personal page: <https://web.media.mit.edu/~monster/>

***
> **/imagine:** *"scan of a medieval manuscript featuring people at a tea party holding tea cups and operating a mechanical structure that represents menstrual cycles, red liquid dripping"* (generated with MidJourney)
>
> [![An imagined medieval kinectic sculpture about menstrual cycles](./images/lu_u_project-medieval.png)](./images/lu_u_project-medieval.png)

***

Some Notes:
- **Fabrication**
- - [The male gaze is alive and well in 3D printing, Cindy Kohtala (2015)](https://blogs.aalto.fi/makerculture/2015/03/17/the-male-gaze-in-3d-printing/)
- - [Strategies of oppression, Cindy Kohtala (2018)](https://blogs.aalto.fi/makerculture/2018/04/29/strategies-of-oppression/)
- **Menstrual health & Mediating taboos**
- - [Chapter 16 Introduction: Menstruation as Embodied, Tomi-Ann Roberts (2020)](https://www.ncbi.nlm.nih.gov/books/NBK565600/)
- - [Taboos in health communication: Stigma, silence and voice (2022)](https://journals.sagepub.com/doi/full/10.1177/2046147X211067002)
- - [Taboos and Myths as a mediator of the Relationship between Menstrual Practices and Menstrual Health](https://academic.oup.com/eurpub/article/31/Supplement_3/ckab165.552/6405705?login=false)
- **Material (3D printed clay & glass blowing)**
- - [3D Printed Clay and Glass Blowing](https://www.instructables.com/3D-Printed-Clay-and-Glass-Blowing/)
- - [Clay 3D Printing Design Guide](https://www.architecture.yale.edu/advanced-technology/tutorials/32-clay-3-d-printing-design-guide)
- **Wearable, machine learning, heart rate measurement**
- - [Smart wearable devices in cardiovascular care: where we are and how to move forward](https://www.nature.com/articles/s41569-021-00522-7)
- - [Breathing Monitoring and Pattern Recognition with Wearable Sensors](https://www.intechopen.com/chapters/66828)
- - [Emerging sensing and modeling technologies for wearable and cuffless blood pressure monitoring](https://www.nature.com/articles/s41746-023-00835-6)