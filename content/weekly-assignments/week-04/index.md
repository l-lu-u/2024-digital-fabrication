+++
title = 'Week 04: Computer-Aided Design'
date = 2024-02-06T13:42:01+02:00
+++
*Vector, Raster, and 3D Object* 

<!--more-->

***

The documentation of this week consists of three parts:
1. Vector drawing in Inkscape,
2. Raster graphic editing in Gimp
3. 3D modelling tools exploration

For graphic editing, I am proficient in using Photoshop Illustrator. Yet, I want to explore (and support) open source alternatives to Adobe Creative Suite. 

***

## Part 1. Vector drawing in Inkscape
Software used: [**Inkscape**](https://inkscape.org/)
Font used: **Arial Black**

[![Vector drawing in Inkscape](./images/inkscape.jpg)](./images/inkscape.jpg)
*>> Vector drawing in Inkscape - A sign for my final project: "AH, BLOOD (AGAIN)." <<*

For the vector drawing assignment, I created a signage for my final project about menstruation. The style of the drawing takes the inspiration of "WARNING" signs. Within the sign, a standing stick figure is pondering about the stain on the chair - the moment of realising the menstrual blood leaks. The chair is drawn using **Pen Tool + Rectangle Tool**, while the stick figure is a combination of a circle (using **Cicle/Arc Tool**) plus lines that with different weight using **Pen Tool**. When finalising the drawing for export, I expanded the stroke through **"Path -> Stroke to Path"** and the text through **"Path -> Object to Path"**.

It did take some time to adjust to Inkscape (from Adobe Illustrator), especially about the shortcut keys. Yet, with the experience working in Illustrator, I could "imagine" what can be possibly done in a vecgtor drawing software and look for the functions accordingly. In sum, I like working with Inkscpae in general. The interface is intuitive, I especially like the right panel that I can access my frequent operations easily. In addition, there are many tutorials available from the community, which helps me to explore more possibilities with this tool. I also like that the working file is saved in **".svg"** format, which allows me to edit or open it in other softwares directly (e.g., Figma, Adobe Illustrator, Fusion 360). It is also easier to share the working file with others without worrying if they have the compatible software installed or not. However, we thus can understand how a "specific format" (**".ai"**) can create exclusivity and dependency in the industry.

One thing I missed this time in Inkscape is the "artboard" in Adobe Illustrator. By creating multiple boards or frames or pages (however we want to call it), one can easility create multi-page documents or variants for one design. After finishing this assignment, I discovered this this **["Multipage"](https://wiki.inkscape.org/wiki/Multipage)**, an Inkscape wiki page on creating multiple pages. I will try this out in my next adventure with Inkscape.

Regarding the drawing itself, it is meant to approach an often stigmatised topic, "menstruation", with a (maybe awkward) sense of humour. When I showed the draft to people working around me in the homebase, whoever had period in their life immediately understood the subject and share with me how much they can relate to it. Interestingly, the meaning of the image appeared to be slightly obscure for many men.

***

## Part 2. Raster graphic editing in GIMP
Software used: GNU Image Manipulation Program [**(GIMP)**](https://www.gimp.org/), I was pondering between Krita and GIMP, but Krita seems to be suitable for digital painting, while Gimp fits the purpose of image editing.

[![Raster image](./images/gimp.jpg)](./images/gimp.jpg)
*>> Raser image editing in GIMP: Inverted hydrangea (photo by me) <<*

The image I am editing is a photo of hydrangea (or hortensia) flowers taken by me in the autumn of 2020. I needed to edit this picture due to some request for cover art, and I decided to seize the opportunity of learning new software when exprimenting with the picture.

To achieve the current effect, I first duplicated the original image and created the film photo grainy effect on the new layer by applying **"Filers -> Noise -> CIE lch Noise"** ([GIMP Documentation here](https://docs.gimp.org/2.10/en/gimp-filter-noise-cie-lch.html)), and then adjusted the contrast through **"Colors -> Adjust Color Curves"**. 

I wanted to keep only the dried flowers coloured while the rest in black and white, so I duplicated a layer and desaturated the photo through **"Colors -> Saturation"**. On the monochrome layer, I used the **"Eraser Tool"** to reveal the coloured layer underneath.

[![GIMP Process](./images/gimp--process.jpg)](./images/gimp--process.jpg)
*>> Raser image editing in GIMP - process <<*

When tuning the image, I inverted the coloured layers just as a test, yet the image looks quite interesting, so I decided to keep the effect. Below is the edited image:

[![GIMP sample](./images/gimp--sample.jpg)](./images/gimp--sample.jpg)

I feel quite comfortable to work with GIMP even though I previously used mostly Adobe Photoshop or Lightroom for graphic editing. Interface wise, both Photoshop and GIMP are intuitive enough for me to navigate around. Yet, Photoshop is much more resource-intensive than GIMP. Though GIMP may not have all the features or tools right after installation, the open-source nature allows one to modify or add plug-ins.

There are a few downsides of GIMP. One is its limitation in file format. **".XCF"** file seems not to be commonly supported by image editing softwares. Also, GIMP cannot open **".RAW"** files directly, I may try out the plugin **[UFRaw](https://ufraw.sourceforge.net/Install.html)** next time for photo editing. Furthermore, GIMP lacks of non-destructive editing methods like **smart object** or **adjustment layers**. For example, when I did the weekly assignment, I had to awkwardly duplicate the layers as my backups. For me, this might be the biggest downside of doing more complicated image editing in GIMP.

***

## Part 3. 3D modeling
I had very limited experience in 3D modeling and desiging for 3D objects. There are a few programs I have used a little bit before:
- **Rhino**: Used twice during my bachelor study. Once for modeling a 3D-printed lamp, once for modeling a very curvy box for package design rendering. The interface feels a bit confusing, but I want to try out the **Grasshopper** - some of the generative pattern design look absolutely fun. For example, [this work](https://www.youtube.com/watch?v=K5QsZP8Ivfk) that "translates" sound both visually and (possibly) physically in Processing and Rhino (the process is documented [here](https://issuu.com/iuritrombini/docs/remix_revealing_unintended_arrangem)). 
- **SolidWorks**: Used a few times when desiging supporting parts for installations. I personally feel SolidWorks is much easier to navigate around than Rhino. 
- **Blender**: I have recently started modeling 3D objects for graphics (as a hobby) in Blender. Yet, I wonder how I can use Blender for 3D printing.

In sum, I know very little about 3D modeling yet I am very curious about many things. Since I want to create a curvy and smooth vessel (that resembles human organs) for my final project, I want to learn how to use the sculpt mode in **Fusion 360** for this assignment (and possibly use it for my final project as well). Following this tutorial [**How to Sculpt in Fusion 360 | Beginner Concepts Explained**](https://www.youtube.com/watch?v=zSbJGnkPnmc), by [Product Design Online](https://www.youtube.com/@ProductDesignOnline), I modeled this watering can:

[![Watering Can STL](./images/watering-can.jpg)](./images/watering-can.jpg)
*>> A watering can modeled according to a tutorial <<*

Through this process, I have learnt quite some fun techniques to edit the form for sculpted bodies. For the handle, it created a **"sweep"** of a circle (sketched on a construct plane) following a spline path from the watering can body to the top.

[![Sweeping the handle](./images/fusion--sweeping-handle.jpg)](./images/fusion--sweeping-handle.jpg)
*>> Sweeping along the spline path <<*

For the sprout, it is **lofted** with two ellipses (bigger one lower, smaller one higher). The two ellipses were sketched on construct planes at the same angle - to do so, we can use **Construct -> Offset plane** and set the offset distance accordingly.

[![Lofting the sprout](./images/fusion--lofting-sprout.jpg)](./images/fusion--lofting-sprout.jpg)
*>> Lofting with two ellipses <<*

**"Bridge"** command was used to merge the ends when sculpting the form. For example, to create a curvy and smooth connection from the handle to the watering can body, I first subdivided the faces on the watering can, and then deleted part of it to create a hole on it. Afterwards, I selected the ends of the handle and the body and applied the bridge command to merge them together.

[![Bridge command](./images/fusion--bridge-command.jpg)](./images/fusion--bridge-command.jpg)
*>> Using bridge command to merge the handle to the can body <<*

After finishing the form, I applied the **"Thicken"** operation to add the thickness to the sheet material.

[![Thicken](./images/fusion--thicken.jpg)](./images/fusion--thicken.jpg)
*>> Apply 3mm soft thickness to the form <<*

In sum, Fusion 360 is quite smooth and understandable to work with. The amount of tutorials available is another plus point.

### In addition, I tried a bit FreeCad (as it's open source)
[FreeCAD for beginners #41 Start Up Macro that everbody should have](https://www.youtube.com/watch?v=ECPluooiKpg), by Adventures in creation
In FreeCard, a **Macro** refers to a list of Phython commands that can be used to reproduce complex actions. Here is the wiki page about "Macros" on FreeCAD Documentation site: <https://wiki.freecad.org/Macros>.

[![FreeCAD Macro](./images/freecad--macro.jpg)](./images/freecad--macro.jpg)
*>> Creat the start up macro in FreeCAD <<*

By following through this tutorial, I gained an overall impression of FreeCAD - its interface and some of its features. I like that one can create and edit a series of actions through scripting. Yet, I am not too sure to use it now for my final project. Just as I mentioned above in the **Inkscape** & **GIMP** sections, I feel it might be easier to learn about the basics in a well-developed software and then I can imagine "what can/should be viable" in a (relatively) more obscure interface/program. 🤔

### Resources
- (Regarding NURBS & Mesh) NURBS vs Mesh Modeling: Optimizing Design Workflow: <https://www.neuralconcept.com/post/nurbs-vs-mesh-modeling-optimizing-design-workflow>
- (Something cool, Rhino + Processing) Remix: Revealing Unintended Arrangements: <https://issuu.com/iuritrombini/docs/remix_revealing_unintended_arrangem>
- (Blender for 3D printing) Learn Blender for 3D Printing - Complete Quick and Easy Guide (Beginner): <https://www.youtube.com/watch?v=rN-HMVTB7nk>
- Self-paced learning for Fusion: <https://help.autodesk.com/view/fusion360/ENU/courses/>
- 3D Differential Growth in Blender!: <https://www.youtube.com/watch?v=XR_vRtzH4GY>