+++
title = 'Week 06: Individual Assignment - Electronics Production'
date = 2024-02-19T14:42:01+02:00
+++
*Circuit board milling & soldering* 

<!--more-->

***

{{< rawhtml >}} 
<video width=100% controls autoplay>
    <source src="https://l-lu-u.gitlab.io/2024-digital-fabrication/videos/week06--outcome.mp4" type="video/mp4">
    Your browser does not support the video tag.  
</video>
{{< /rawhtml >}}

The PCB production process consists of two main parts:
1. milling the circuit board,
2. soldering the electronic components.

**"Milling"** refers to a machining process that uses rotary cutters (i.e., "bits") to remove material by pressing the cutter/bit into a workpiece. In **P**rinted **C**ircuit **B**oard **(a.k.a. "PCB")**, the milling process removes areas of copper and thus leaves the conductive structure as the circuit.

For this week's assignment, we are using an existing PCB design, which can be found here: [Tarantino](https://gitlab.fabcloud.org/pub/programmers/tarantino), under the **/Gerber** folder:
- Tarantino-B_Cu.gbr *// the back*
- Tarantino-F_Cu.gbr *// the front*
- Tarantino-PTH-drl.gbr *// parts needed to be drilled*

The milling machine I used is **Roland SRM-20**. In my post for the group assignment, I documented the introduction about using this machine and soldering at the Aalto Fablab workbench. Here is the link to the documentation: <https://l-lu-u.gitlab.io/2024-digital-fabrication/weekly-assignments/week-06--group/>

Below is the documentation about how I completed my board for the individual assignment, which consists of 3 parts:
1. Mill the board,
2. Solder the components,
3. Test it the code.

As I have documented most of the details about operating SRM-20 in my group assignment writing, I will avoid the repetition and focus on my personal reflection in this post.

***

## Part 1. Mill the circuit board

As mentioned in the group assignment post, I milled the circuit board following these steps:
1. Prepare the copper clad board for the machine,
2. Prepare the circuit board paths (in **Gerber** format) in software **CopperCAM**,
3. Set up the the machine using **VPanel** (the controller of SRM-20) and install the milling bit,
4. Calibration: Set the **Z-origin** for the bit,
5. Calibration: Set the **X/Y-origin** for the bit,
6. Import the milling file in VPanel,
7. Change the tool for a different type of operation,
8. Done & clean-up <3

[![The milled board](./images/milling--my-board.jpg)](./images/milling--my-board.jpg)

Notice the size difference along the drilled holes. When conducting the drilling and cutting operation, the milling bit started gradually moving upwards and thus did not drill through the board after a while. With the help from Saskia, we tightened the bit more using the hex key.

It takes a few trials to grasp how much force is needed when fixing the bit. For me, I am now judging by the very subtle "click" when turning the key.

***

## Part 2. Solder the components

Before actually installing the rivets on my board - although it is single-sided, I still want to try out and know how this part works - I practiced on a failed board.
[![Rivet practice](./images/soldering--rivet-practices.jpg)](./images/soldering--rivet-practices.jpg)

It went worked well.

After installing the rivets, I started the soldering process with references to the schematic diagram (printed out by Burak) and the example board soldered by Kris.
[![Soldering reference](./images/soldering--reference.jpg)](./images/soldering--reference.jpg)

When observing these references, I decided to proceed my soldering following this order (to lower the risk of making difficult-to-fix mistakes):
1. Spiky headers to the microcontroller (i.e., Seeed Studio XIAO RP2040),
2. Small components with polarity (i.e., LEDs),
3. Small components without polarity (i.e., resistors),
4. The rest except the sockets for XIAO,
5. The sockets.

I made one mistake - before placing the component, I applied some tin on all the contact points, which made it harder for the component to stay flat during the process. As a result, I had to hold them really tightly with the tweezer. Fortunately, it turned out fine.

And here it is - the populated board (as you can see, the LEDs and the resistors are a bit slanted):
[![Soldering result](./images/soldering--finished.jpg)](./images/soldering--finished.jpg)

Terms:
- **SMD:** Surface Mounted Device
    - Space efficiency
- **THT:** Through Hole Technology
    - Good for fast prototyping

***

## Part 3. Test it with the code ("hello_tarantino.ino")

The code we are using to test the board can be found here: <https://gitlab.fabcloud.org/pub/programmers/tarantino/-/blob/main/Arduino/hello_tarantino/hello_tarantino.ino?ref_type=heads> With a working board, this code will let the button function as a toggle to turn on and off the LED.

And, the detailed documentation about using Seeed Studio XIAO RP2040 with Arduino is here: <https://wiki.seeedstudio.com/XIAO-RP2040-with-Arduino/>

After launching the Arduino IDE, I need to add the **Seeed Studio XIAO RP2040 board package**. To do so, I clicked **"Arduino IDE > Preferences"**, and fill in the **Additional Boards Manager URLs** field with the url below:
```
https://github.com/earlephilhower/arduino-pico/releases/download/global/package_rp2040_index.json
```

Now, we can see **"Raspberry Pi Pico/RP2040"** package appear in the boards manager if we search for **"XIAO"**, click **"Install"**.
[![Load the board](./images/testing--load-the-board.jpg)](./images/testing--load-the-board.jpg)

After selecting the board and port, I uploaded the code. As you have seen in the video at the very beginning of this post, it works.

***

That's it <3

But, I also had some other (seemingly irrelevant) thoughts ⬇️ ⬇️ ⬇️

## Machine as an external/extended body and/or an interface between the body and the material
During the past two weeks, I had hands-on experience in operating vinyl cutting machine and PCB milling machine for the first time. Interestingly, I found these processes somewhat similar to printmaking. For example, when designing for heat transfer vinyl, we need to mirror the graphic as the pattern will be placed face down onto the print surface - just like woodblock printing. When removing the unused parts, it also feels like cutting the woodblock. For PCB production, the copper clad board of course reminds me of the copperplate for printmaking: etching or directly pressing the cutter into the material. It gets funny to imagine SRM-20 as an "electrical Outi Heiskanen."

It is fairly obvious why they are similar - the subtractive process, the material (i.e., copper), and some of the logic. Yet, for me, they give quite opposite feelings - I usually find it very calming in printmaking workshop holding a knife to cut wood or copperplate, but the millinng and cutting machines always appear to be intimidating. 
[![Comparison between PCB and printmaking](./images/printing--compare.jpg)](./images/printing--compare.jpg)
*>> The process, the tool, and the material <<*

It is intriguing to comtemplate why machining with electronics feels more stressful for me comparing to the handcraft with pigment and paper. Pushing a neddle forcibly into a copperplate with bare hands in fact is exhausting - while machines are capable to store detailed and complex information, to calculate, to continue with consistency without feeling tired. 

However, we - or, I am more aware of how (un)reliable my body is when applying force onto a material: the nuance between "just right" and "not enough" -> the tacit knowledge **(a force measuring wrench would help)**. Machines are external - some standalone entities existing outside of my own body. When introducing a machine to the workflow, we externalise part of the knowledge to the machine but we are now obligated to acquire a new set of knowledge - the procedure of operating the machine and its mechanism behind. Hence, the machine works as an interface between us (our bodies) and the material that we intend to manipulate. Only when a disruption happens (e.g., when the cutting bit does not cut through the circuit board), we will resume our contact with the material again (e.g., pressing the bit closer to the surface or feeling the "click" when screwing the hex key).

I will probably read Judith Butler.

[![Comparison between PCB and printmaking](./images/printing--outcome.jpg)](./images/printing--outcome.jpg)
*>> The "print"<<*