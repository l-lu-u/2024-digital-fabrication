+++
title = 'Week 10: Electronics Design'
date = 2024-03-13T14:42:01+02:00
+++
*More PCBs* 

<!--more-->

***

For this week, Rui has created the documentation of our group introduction to the test equipment at the lab: <https://zruii.gitlab.io/digital-fabrication/group_documentation.html>

The individual assignment is to design and make a circuit board using KiCad (or equivalent electronics design automation (**EDA**) software). The documentation will be divided into two, wait, actually three parts:
0. (Preparation) Learning from the example demonstrated in class,
1. Designing,
2. Milling & soldering.

***
## Part 0. (Preparation) Learning from the example demonstrated in class
By following the demonstration in class, I completed a PCB design from the schematic to the board layout. Here is the layout in the Gerber viewer, showing both the copper tracks (engraving) and the mounting holes (drilling):

[![Gerber view of the baord created based on the tutorial](./images/demo--gerber.jpg)](./images/demo--gerber.jpg)
*>> Gerber Viewer: The PCB design done by following the tutorial from the class <<*

[![Schematic of the baord created based on the tutorial](./images/demo--schematic.jpg)](./images/demo--schematic.jpg)
*>> The Schematic <<*

The EDA software used in **[KiCad](https://www.kicad.org/)**, a typical KiCad workflow is to design the **schematic drawing** first and then move on to the **circuit board layouting**. Below is my simplified explanation of the terms mentioned:
- **schematic**: a *symbolic* representation of the circuit
- **board layout**: a *physical realization* of the schematic, on the circuit board layout,
    - each electronic **component** will be represented as the **footprints** (a set of copper pads that match the pins on a physical component), while 
    - the **connections** will be physicalised as the **copper tracks**, which are drawn based on both the connections and the electrical considerations, such as trace resistance, controlled impedance requirements, crosstalk, etc.

**from the schematic to the board:** when the schematic is complete and the design has passed an **electrical rules check (ERC)**, the design information in the schematic is transferred to the board editor and layout begins.

Note that, **!!! KiCad is project-based**, thus, opening a board outside of its associated project may result in missing design information

Instead of the default library, I installed the **Fab Electronics Library for KiCad**, a full inventory can be found here: <https://inventory.fabcloud.io/> 

For small and hard-to-solder chips (e.g., Accelerometer_ADXL343), use **breakout board**.

For the schematic drawing, here is the **rule of thumb:**
- top to bottowm -> positive to negative voltage
- left -> input, right -> output

Below are some notes taken from the lecture:
> **Power flag** - denote where the power comes from (line)
> **Connector Pin Header** - in order to programme the board?
> UPDI chip is internally connected, so we just need to connect one set of the UPDI+GND+VCC among the 6 pins, just need to draw the "no-connection flags" on the unconnected pins.
> Resistor -> water analogy
> LED Current Limiting Resistors -> forward current & forward voltage
> In KiCad, select a component and "D" -> open datasheet -> find out optimal forward current and voltage. In the demo tutorial, the calculated resistor value is 80 Ohm, we choose the closest value from the inventory -> 100 Ohm; if we want it to be (more) precise, we can also make a series connection of 1 x 49.9 Ohm + 3 x 10 Ohm resistors.
> The forward voltage may change depending on the color of the voltage -> blue ones require (a bit) higher voltage than other colors
> In the tutorial, pin 7 (BTN) is connected through a pull-up resistor in the chip, when the switch is off, the BTN is connected to the 5V, when the switched is pressed, the pin 7 is connected to the ground and read as 0.
> Pull-up vs pull-down resistor
> Serial -> communication between two boards
> optimal track width for milling machine at Aalto FabLab is 0.4mm (using V-bit)
> **CHECK** -> Auto-routing in KiCad

**LED Current Limiting Resistors:** <https://www.sparkfun.com/tutorials/219>

### After all, what is this "GERBER" format
(Reference: [What Is A Gerber File and How Are They Used in the PCB Fabrication Process?](https://resources.altium.com/p/what-gerber-file-pcb-fabrication-process))

...the "front-end task" in the PCB fabrication process:
> Gerber files store all of the shape and location data for every element in a PCB layout. Each layer will be placed into its own Gerber file -> for each step in the fabrication and assembly process (e.g., engrave/drill/cut, front/back). For example, in the tarantino board production, we have three Gerber files:
> - Tarantino-B_Cu.gbr
> - Tarantino-F_Cu.gbr
> - Tarantino-PTH-drl

(AND, what is CAM? -> **Computer-Aided Manufacturing**)

***
## Part 1. Designing PCB using KiCad

> **Minimum requirement:**
> - a LED, 
> - a button, and 
> - a 2x2 connector for serial communication with other boards
> - break out all the unused pins of the microcontroller you are using as the core *(What does this mean???)*
> - calculate the ideal value of a current limiting resistor for the LED used

Tool used: [**KiCad**](https://docs.kicad.org/8.0/en/getting_started_in_kicad/getting_started_in_kicad.html) *(Note: for beginners, don't download the original library, but the fab library)*

Core component: XIAO board

[![Metratronic Test 1 Schematic](./images/metratronic--test-1-schematic.jpg)](./images/metratronic--test-1-schematic.jpg)
*>> Test 1: Schematic <<*

[![Metratronic Test 1 PCB](./images/metratronic--test-1-pcb.jpg)](./images/metratronic--test-1-pcb.jpg)
*>> Test 1: PCB <<*

[![Metratronic Test 1 Why the shape](./images/pcb--why-the-shape.jpg)](./images/pcb--why-the-shape.jpg)
*>> Test 1: why the shape <<*

(microcontrollers comparison): 
- XIAO, 
- ATtiny, or 
- D11C

> UART vs UPDI


Copper filled zone to connect to the ground -> **REMEMBER TO hit "B" after selecting the zone!!!**

Some experiment
- **Customise shape**: I designed the shape in Inkscape, save as a **".svg"** file, and then import the graphic to **"Edge Cut"** layer in KiCad,
    - Tutorial here: [How too design a complex and custom shape PCB with images in KiCad](https://www.theamplituhedron.com/articles/How-to-design-a-complex-and-custom-shape-PCB-with-images-in-KiCad-and-manufacture-it-with-PCBWay/)
- **Text**: "MĒTRA-TRONIC" -> Greek mētra "womb" (from mētēr "mother")
    - Add the text in KiCad, and set it to be engraved as centerline in CopperCAM
    - I didn't check the **"Knockout"** option for the text, which let it leave a blank box around the text in the PCB layout view. But on the actual milling, it is just engraved on the copper zone. How does the "Knockout" work (shall we use it or, not?)


*** 
## Part 2. PCB milling and soldering

*[11th April, 2024 (Thursday)]*

[![Metratronic Test 1](./images/metratronic--test-1.jpg)](./images/metratronic--test-1.jpg)
*>> Test 1: Milling <<*

[![Metratronic Test 1](./images/metratronic--test-1--not-cut-thru.jpg)](./images/metratronic--test-1--not-cut-thru.jpg)
*>> The engraving bit did not cut through at the first trial <<*

[![Metratronic Test Red Lines](./images/metratronic--test-1--coppercam-red-lines.jpg)](./images/metratronic--test-1--coppercam-red-lines.jpg)
*>> Red lines in CopperCAM <<*

I also have questions for these red lines, does it means I need to right click and set them all as contours?

To-do for the iteration:
- make the pad more compact -> less area and then smaller copper pad needed, flatter for milling
- check the depth?

## Part 3. Code
```cpp
#include <Adafruit_NeoPixel.h>

#define BTN D9
#define NUM_PIXELS 1

int powPin = NEOPIXEL_POWER;
int neoPin = PIN_NEOPIXEL;
int boardLed = PIN_LED_R;
int ledCount = 2;
int cyclePhase[3] = {7, 14, 7};
int ledPins[2] = {D0, D3}; // surface mount LEDs (D6 is not connected to the ground)
int microSteps = 16; // 16 means 1/16, 32 means 1/32 etc.
bool ledState = LOW; // HIGH, true, and 1 mean the same
bool btnState = HIGH; // button is high as it is connected to 3.3V via a pull-up resistor
bool inCycle = LOW;
bool isWaiting = HIGH;

Adafruit_NeoPixel pixels(NUM_PIXELS, neoPin, NEO_GRB + NEO_KHZ800);

unsigned long previousMillis = 0;
unsigned long interval = 1000; // interval in milliseconds

void setup() {
  pixels.begin();

  pinMode(PIN_LED_R, OUTPUT);
  pinMode(powPin, OUTPUT);
  pinMode(BTN, INPUT_PULLUP);

  for (int i = 0; i < ledCount; i++) {
    pinMode(ledPins[i], OUTPUT);
  }

  // Set the initial state of our LEDs
  for (int i = 0; i < ledCount; i++) {
    digitalWrite(ledPins[i], LOW);
  }

  // digitalWrite(PIN_LED_R, HIGH);

  digitalWrite(powPin, HIGH);
  digitalWrite(boardLed, LOW);

  Serial.begin(9600); // open the serial port at 9600 bps
}

void loop() {

  // Non-blocking button press detection
  bool btnReading = digitalRead(BTN);
  if (btnReading != btnState) {
    if (btnReading == LOW) { // LOW means button is pressed
      Serial.println("Button pressed >>>");
      isWaiting = LOW;
      btnState = LOW;
      inCycle = HIGH;
    } else {
      btnState = HIGH;
    }
    delay(50); // debounce
  }

  if (isWaiting) {
    waiting();
  } else if (inCycle) {
    // Serial.println("Entering your cycle >>>");
    cycle();
  }
}

void waiting() {
  static unsigned long waitMillis = 0;
  static int brightness = 0;
  static bool increasing = true;
  unsigned long currentMillis = millis();

  if (currentMillis - waitMillis >= 10) {
    waitMillis = currentMillis;
    if (increasing) {
      brightness++;
      if (brightness >= 50) increasing = false;
    } else {
      brightness--;
      if (brightness <= 0) increasing = true;
    }

    int r = random(0, 255);
    int g = 0;
    int b = 0;

    pixels.clear();
    pixels.setPixelColor(0, pixels.Color(r, g, b));
    pixels.setBrightness(brightness);
    pixels.show();
  }
}

void cycle() {
  static int dayCount = 0;
  static int phase = 1;
  static unsigned long cycleMillis = 0;
  unsigned long currentMillis = millis();

  pixels.clear();
  pixels.setPixelColor(0, 0);
  pixels.setBrightness(0);
  pixels.show();

  if (currentMillis - cycleMillis >= 800) {
    cycleMillis = currentMillis;
    dayCount++;
    Serial.print("Day count: ");
    Serial.println(dayCount);

    digitalWrite(boardLed, HIGH);
    delay(500); // brief delay to simulate blink
    digitalWrite(boardLed, LOW);

    if (phase == 1 && dayCount >= cyclePhase[0]) {
      phase++;
      ovulation();
    } else if (phase == 2 && dayCount >= cyclePhase[0] + cyclePhase[1]) {
      phase++;
      bleed();
    } else if (phase == 3 && dayCount >= cyclePhase[0] + cyclePhase[1] + cyclePhase[2]) {
      Serial.println("End of cycle >>>");
      inCycle = LOW;
      isWaiting = HIGH;
      phase = 1;
      dayCount = 0;
      digitalWrite(boardLed, LOW);
    }
  }
}

void ovulation() {
  Serial.println("Ovulation >>>");
  
  int ovary = random(0, 2);
  Serial.print("Ovary: ");
  Serial.println(ovary);

  for (int i = 0; i < 5; i++) {
    digitalWrite(ledPins[ovary], HIGH);
    delay(100);
    digitalWrite(ledPins[ovary], LOW);
    delay(100);
  }
}

void bleed() {
  Serial.println("Bleeding >>>");
  int r = 255;
  int g = 0;
  int b = 0;
  for (int i = 0; i < 255; i++) {
    pixels.clear();
    pixels.setPixelColor(0, pixels.Color(r, g, b));
    pixels.setBrightness(i);
    pixels.show();
    delay(10);
  }
  for (int i = 255; i > 0; i--) {
    pixels.clear();
    pixels.setPixelColor(0, pixels.Color(r, g, b));
    pixels.setBrightness(i);
    pixels.show();
    delay(10);
  }
}
```

***

## Notes from global lecture
The global lecture consists of two parts:
- Components (& how they are drawn)
- EDA(electronics design automation ) softwares, like KiCad

### Components (& how they are drawn)
- Ribbon cable
- IDC - insulation displacement connector
- Slide switch
- Resistor (I = V/R, i.e., current -> voltage over resistor)
- Capacitor (C = Q/V, i.e., charge over voltage, unit: Farad), an AC signal can go through while a DC signal cannot
    - unpolarised <-> polarised
- Crystal: a piezoelectric material inside

Inspiration:
- Fab Academy 2023 -> [Kai Zhang](https://fab.cba.mit.edu/classes/MAS.863/Harvard/people/Kai/index.html#about), instagram: [@zalvink_](https://www.instagram.com/zalvink_/)


***

## Totally irrelevant but...
**QUESTION QUESTION!!!** How come we are using the word "workflow" so much now and/or here (in CAD)? (Or is it just because, I am finally in some "discourse"...?)