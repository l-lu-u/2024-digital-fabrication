+++
title = '🩸 Final Project 🩸'
date = 2024-01-18T13:42:01+02:00
+++
*Final project documentation: from ideas to something done(?)* 

<!--more-->

***

## Ongoing Progress
{{< rawhtml >}} 
<video width=100% controls>
    <source src="https://l-lu-u.gitlab.io/2024-digital-fabrication/videos/spill--motor-moving.mp4" type="video/mp4">
    Your browser does not support the video tag.  
</video>
{{< /rawhtml >}}

## I. 🩸 Project concept: "SPILL! 🩸"

For digital fabrication final, I plan to revise and polish the project *SPILL* (from course Physical Computing 2023 Autumn). Here is the link to [previous course documentation and reflection page](https://newmedia.dog/students/2023/lu-chen/physical-computing/final-project/).

*SPILL* is a kinetic installation that represents menstrual cycles mechanically with a set of tea wares found in a recycling centre. Through this work, I intend to depict some absurdity about this common (yet often taboo) biological process as well as to invite the audience to openly discuss about their exprience and understanding.

The initial sketch and the exhibited version in December 2023 are shown below:
[![Initial sketch and the exhibited prototype in 2023](./images/spill--version-1.jpg)](./images/spill--version-1.jpg)

And here is the measurement from this prototype, which can be used as a reference for the upcoming revision.
[![Prototype dimensions in 2023](./images/spill--prototype-dimension.jpg)](./images/spill--prototype-dimension.jpg)

Here are some of my goals (or wishes) for this iteration:
- **Form development:** 
- - Design the supporting frame to be more organic looking, the overall dimension should be able to fit on a tea table;
- - Design the clamping structures for motors and cups with a similar organic look and feel with the overall structure.
- **Interaction Design:**
- - Visualise and/or sonify the waiting time, which can be designed according to the menstrual cycle as well;
- - Use input like the audience’s pulse or breathing (28 counts maybe?) to control the "period duration" instead of the online dataset. 
- **Mechanism:**
- - I think it is possible to reduce at least one motor to control the movement for both ovaries, so that the installation can be more efficient.
- **Robustness:**
- - Revise the spool structure and the string length - for the current version, the tampon is controled with a string wounded on a spool, while the spool is attached to a stepper motor on top of the frame. However, the string is often entangled on the spool. Or, there can be an alternative way to dip and lift the tampon;
- - Review the usage of magnetometer. Currently, I use 0.65 for either x/y/z direction as the threshold to stop the spooling, I can either stablise the tampon (and the spoon), or investigate more about the ways of working of magnetometer;
- - A self-calibration process to be;
- - More secured wiring.
- **Experimentation:**
- - Soft robotics;
- - 3D printing -> mold casting -> ceramics + glass;
- - 3D clay printing 

A visual moodboard for now (January, 2024):
[![Visual Moodboard](./images/spill--visual-moodboard.jpg)](./images/spill--visual-moodboardt.jpg)

### Research & Sources used in Version 1
- **Fabrication**
    - [The male gaze is alive and well in 3D printing, Cindy Kohtala (2015)](https://blogs.aalto.fi/makerculture/2015/03/17/the-male-gaze-in-3d-printing/)
    - [Strategies of oppression, Cindy Kohtala (2018)](https://blogs.aalto.fi/makerculture/2018/04/29/strategies-of-oppression/)
- **Menstrual health & Mediating taboos**
    - [Chapter 16 Introduction: Menstruation as Embodied, Tomi-Ann Roberts (2020)](https://www.ncbi.nlm.nih.gov/books/NBK565600/)
    - [Taboos in health communication: Stigma, silence and voice (2022)](https://journals.sagepub.com/doi/full/10.1177/2046147X211067002)
    - [Taboos and Myths as a mediator of the Relationship between Menstrual Practices and Menstrual Health](https://academic.oup.com/eurpub/article/31/Supplement_3/ckab165.552/6405705?login=false)
- **Material (3D printed clay & glass blowing)**
    - [3D Printed Clay and Glass Blowing](https://www.instructables.com/3D-Printed-Clay-and-Glass-Blowing/)
    - [Clay 3D Printing Design Guide](https://www.architecture.yale.edu/advanced-technology/tutorials/32-clay-3-d-printing-design-guide)
- **Wearable, machine learning, heart rate measurement**
    - [Smart wearable devices in cardiovascular care: where we are and how to move forward](https://www.nature.com/articles/s41569-021-00522-7)
    - [Breathing Monitoring and Pattern Recognition with Wearable Sensors](https://www.intechopen.com/chapters/66828)
    - [Emerging sensing and modeling technologies for wearable and cuffless blood pressure monitoring](https://www.nature.com/articles/s41746-023-00835-6)

[![To-do for the final](./images/final--todo.jpg)](./images/final--todo.jpg)
*> 2024.05.02 - A rough outline of what I need to do <*

(on the form: if it is exhibited, how to transport it - divide it into parts)

***

> **/imagine:** *"scan of a medieval manuscript featuring people at a tea party holding tea cups and operating a mechanical structure that represents menstrual cycles, red liquid dripping"* (generated with MidJourney)
>
> [![An imagined medieval kinectic sculpture about menstrual cycles](./images/lu_u_project-medieval.png)](./images/lu_u_project-medieval.png)

***
## II. Research on some problems noticed from the first version (December, 2023)
One of the major problems is that the cotton string keeps getting tangled on the spool. From a fishing forum, I learnt about some possible reasons that might have resulted to this problem: [3 Reasons Your Fishing Line Keeps Getting Tangled & How To Prevent It](https://panfishnation.com/fishing-line-keeps-getting-tangled/).

Among the mentioned tanglement causes, **over-spooling** seems like the most applicable one in my case. I was using excessively long cotton string for my installation. 

Besides, I wondered, would fishing lines with more elasticity work better for this repetitive rolling up and down. 

> *Shorten the string and* 
> *Test with fishing line as well*

**[10th April, 2024 (Wednesday)]**
I came across Odagiri Satoshi's (小田切 知真) crafted mobile online, which can be found here: [Satoshi Odagiri Creates One-of-a-Kind Organic Mobiles from Found Material](https://www.spoon-tamago.com/satoshi-odagiri-kinetic-sculptures/). The mobile's movement on the pivot inspired me to come up an alternative solution for the spooling mechanism. Below is a rough sketch:
[![An alternative for the spooling mechanism](./images/final--sketch-240410-overall.jpg)](./images/final--sketch-240410-overall.jpg)
*>> Sketch of the alternative for the spooling mechanism <<*

[![The movement of each motor](./images/final--spooling-solution-motors.jpg)](./images/final--spooling-solution-motors.jpg)
*>> The movement of each motor <<*
In this solution, there will be 3 motors used (*can it be even less?*):
1. **Motor 1**: to control the string (tied to the tampon),
2. **Motor 2**: to control the long cup ("uterus" + "cervix"), which will tilt to allow the tampon to slide in or move out,
3. **Motor 3**: to control the two small cups ("ovaries"), shake or "highlight" one of them.

Another inspiring example is Zita Bernet and Rafael Sommerhalder's bubble blowing installation. I like how the installation turns the circular movement (of an oscillating fan) into the linear movement for a bubble wand. Here is the link: <https://abakcus.com/video/bubbles/>

**[11th April, 2024 (Thursday)]**
*Why pulse:*
- Pulse (heart rate) -> blood flows through
- Hormonal contraception, both the pills and the intrauterine device (IUD) can increase the risk of blood clots, heart attack, and stroke (link: <https://www.cochrane.org/CD011054/FERTILREG_risk-heart-attack-and-stroke-women-using-birth-control-pills>)

[![Menstruation Cycle](./images/reference--menstrual-cycle.jpeg)](./images/reference--menstrual-cycle.jpeg)
*>> [Menstrual Chart](https://menstrual-chart.blogspot.com/2012/06/menstrual-cycle.html)<<*

*Interaction:*
1. one touches the pulse oximeter, the installation movement and LED blinking starts to synchronise with the pulse, keep the hand on the interface;
2. the countdown starts (from the first day after menstruation ends) >> ovulation (a signal + the countdown) >> thickening of uterine lining (countdown continues) >> menstruation (the tampon drops to the tea cup)

**[18th April, 2024 (Thursday)]**
*Animation curve* as an inspiration to the motor motion and address the string tanglement issue:
- instead of changing the speed linearly from 0 to the max, gradually increasing the speed with acceleration and decelleration.

*Adding tension* to the string (also for preventing the string tanglement)
- check belt pulley mechanism
- idler pulley to add the tension

### II. a. Random Finds

> On "Artificial Womb": <https://en.wikipedia.org/wiki/Artificial_womb>

> Another inspiration is Kelly Dobson's ScreamBody done at the "How to Make (Almost) Anything," link to the documentation: <https://web.media.mit.edu/~monster/screambody/>. The concept of "Wearable Body Organs" inspired me to contemplate more the design of my mechanical menstrual kit.
> Link to Kelly Dobson's personal page: <https://web.media.mit.edu/~monster/>

**!!! To-check:** Toque (Youtube: I found Best Robot Actuator - GYEMS: RMB x8)

### II. b. Advices from Kris:
- Check out Mika's wood branch project,
- Imagine a water tight organic mesh as the container - which can be fabricated using mold and vacuum forming.

## III. Form (& function) development

**[9th May, 2024 (Thursday)]**
Basically, it is a crane.

**[13th May, 2024 (Monday)]**
Actually, it is an arm -> an arm that I can understand <https://grabcad.com/library/robotic-arm-with-nema-17-stepper-motors-1>, or this **[3d Printed Cycloidal Drive for Robot Arms](https://www.youtube.com/watch?v=r2TWC7vTdvs)**.

{{< rawhtml >}} 
<video width=100% controls>
    <source src="https://l-lu-u.gitlab.io/2024-digital-fabrication/videos/spill--gravity-change.mp4" type="video/mp4">
    Your browser does not support the video tag.  
</video>
{{< /rawhtml >}}

*>> Find a point slightly above the gravity center of the cup, make sure the cup can swing freely, when it is empty, it is slightly slanted, when it is filled with some weight, it becomes perpendicular to the XY plane, when it is full, it flips over. <<*

[![Form and function](./images/spill--form-and-function.jpg)](./images/spill--form-and-function.jpg)
*>> How it should work (ideally) <<*

[![A cycle of the installation](./images/spill--cycle.jpg)](./images/spill--cycle.jpg)
*>> A cycle of the installation <<*

**Minimum:**
- Structure to let the **Long cup** rotate freely
- To sense the flip of the **Long cup**
- To move the **Arm #2** up and down
- A basic visual cue (e.g., red light blinking)

**Intermediate:**
- **Arm #1**
- Multi-purpose **Tool holder** that can hold whatever (e.g., cam for taking panorama - so I can reuse another project)

[![Timetable](./images/spill--timetable.jpg)](./images/spill--timetable.jpg)
*>> A rough schedule <<*


(Reference: <https://www.instructables.com/Remotely-Operated-WEBCAM-Laser-Camera-flashlight/>)

Introduction to Timing Belts: <https://www.instructables.com/Types-of-Timing-Belts/>

I modelled a timing belt pulley using FreeCAD by following this tutorial: <https://www.youtube.com/watch?v=IvSCX05URzM>

## IV. Electronic Design

### Components & Datasheet

**Motor controller:**
[TMC2208SILENTSTEPSTICK](https://www.digikey.com/en/products/detail/analog-devices-inc-maxim-integrated/TMC2208SILENTSTEPSTICK/6873626), [Datasheet](https://www.analog.com/media/en/technical-documentation/data-sheets/TMC2202_TMC2208_TMC2224_datasheet_rev1.13.pdf)

To be honest, I think this tutorial from watterott is much more helpful: <https://learn.watterott.com/silentstepstick/pinconfig/tmc2208/>

**Motor:**
[NEMA format 17 Stepper Motors 42BYGH](https://mecheltron.com/en/product/42bygh-stepping-motor)

[![PCB design for motor](./images/spill--pcb-one-motor.jpg)](./images/spill--pcb-one-motor.jpg)
*>> PCB board for one motor <<*

**Power plug:**
[S10KS12 Barrel Connector](https://www.digikey.com/en/products/detail/switchcraft-inc/S10KS12/3909315)
- [How To Solder Barrel Jack Connector](https://www.youtube.com/watch?v=qK0aR6hoFfI)

**Power entry modules without line filter:**
[IEC Appliance Inlet C14 with Fuseholder 1-pole, wired, 6200, Snap-on version](https://www.mouser.fi/datasheet/2/358/typ_6200-1275807.pdf)

***

References: 
- [Affordable Robotic Arm](https://www.instructables.com/DIY-Robotic-Arm-Using-28byj-48-Stepper-Motors-and-/)
- [A Fully Functional Tower Crane Desktop Replica](https://www.hackster.io/tartrobotics/a-fully-functional-tower-crane-desktop-replica-5628fd)


- <https://www.instructables.com/DIY-Robot-Arm-6-Axis-with-Stepper-Motors/>
- <https://hackaday.io/project/185958-rr1-real-robot-one-a-diy-desktop-robotic-arm>
- <https://thegadgetflow.com/portfolio/dobot-robotic-arm-everyone-arduino-open-source/>
- <https://howtomechatronics.com/projects/scara-robot-how-to-build-your-own-arduino-based-robot/>
- <https://roboticsandenergy.com/projects/arduino-projects/robotic-arm/>
- <https://hackaday.com/2020/09/15/open-source-robotic-arm-for-all-purposes/>

- another arm I can understand **[Robot arm Educational and made of acrylic glass Part 1](https://www.youtube.com/watch?v=BuuI0GhlyXw)**, and the making of it **[Robot arm Educational and made of acrylic glass. Part 2 : the making of.](https://www.youtube.com/watch?v=QcshZGzNe7I)**
- and another **[ROBOTICS | Miniature 3-axis robotic arm](https://www.youtube.com/watch?v=yhdL4jz74WM)**

***

## On "Tampon Tax"
- <https://en.wikipedia.org/wiki/Tampon_tax#Activism>
- <https://www.investopedia.com/tampon-tax-4774993>