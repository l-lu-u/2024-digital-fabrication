+++
title = 'Week 13: Molding & Casting'
date = 2024-04-17T14:42:01+02:00
+++
*Postive > Negative > Positive, agak agak, agar agar* 

<!--more-->

***

[![Ang ku kueh jelly](./images/angkukueh--result.jpg)](./images/angkukueh--result.jpg)
*>> A very firm jelly in the shape of Ang Ku Kueh <<*

[![Ang ku kueh jelly](./images/angkukueh--result-alt.jpg)](./images/angkukueh--result-alt.jpg)
*>> Details <<*

This post consists of 4 parts indeed:
- Part 0 - Link to group assignment
- Part 1. - Concept & Modelling
- Part 2. - Mold printing & Casting
- Part 3. - Some reflections on the making & the "exotic object"

## Part 0. Group assignment (ACTUALLY DONE BY GROUP EFFORT)

Group assignment done together with Vytautas & Celeste: <https://l-lu-u.gitlab.io/art-media-research-diary-2024/fab-to-the-lab/week13/>

***

## Part 1. Concept and design - “点心” (“a heartful bit”)

### 1.1 Concept

For the molding and casting, I want to design a mold for making Ang Ku Keuh, a confectionery made from glutinous rice flour skin and sweet (paste-like) fillings. This tortoise shell shaped confectionery is traditional and still very popular in Hokkien (福建) and Henghua (兴化) cultures. Here, you can find even detailed explanations about it:
- **Hokkien (福建)** -> Ang ku kueh (红龟粿), Wikipedia page can be found here: <https://en.wikipedia.org/wiki/Ang_ku_kueh>
- **Henghua (兴化)** -> Ang tuan (红团), Baidu baike page can be accessed here (the equivalent of Wikipedia in China): https://baike.baidu.com/item/%E7%BA%A2%E5%9B%A2/9073018

Traditionally, the tortoise shell pattern on this dessert is casted (or pressed) using carved wooden mold.

[![What is Angkukueh](./images/mold--angkukueh-example.jpg)](./images/mold--angkukueh-example.jpg)
*>> How angkukueh and the mold normally looks like <<*

And below, it is a (relatively new) wooden mold used by my grandma, I 3D scanned it when I was at home during the last winter break.

{{< rawhtml >}} 
    <script type="module" src="https://ajax.googleapis.com/ajax/libs/model-viewer/3.5.0/model-viewer.min.js"></script>
    <model-viewer alt="master mold for ang ku kueh" src="./resource/angkukueh-woodenmold.glb" ar environment-image="" poster="" shadow-intensity="1" camera-controls touch-action="pan-y"></model-viewer>
{{< /rawhtml >}}

*>> A newly made wooden mold owned by my grandma (back home) <<*

### 1.2 Modeling

Tool used: **FreeCAD**

How I imagined my working process to recreate the shape of this confectionery through 3D modelling:
- Loft the curvy surface (Reference tutorial: [FreeCAD For Beginners 36 | Loft Workflows | Faces for reinforcements Loft to Surface for finishing](https://www.youtube.com/watch?v=e6GQJG_T2ew) by MangoJelly Solutions for FreeCAD)
 - Build the profile for lofting in Part Design Workbench, and then create the support structure for surface profile in Surface Workbench
- Map the SVG pattern on the curve surface (Reference tutorial: [Freecad tips and tricks (#4.2): Mapping a logo to a curve surface](https://www.youtube.com/watch?v=x8ObvOhdiuE) by Tabletop Robotics)

[![Loft](./images/angkukueh--cad.jpg)](./images/angkukueh--cad.jpg)
*>> The lofted shape plus the hexagonal curvy surface on top <<*

Creating the tortoise shell pattern is also very fun. I drew the pattern using **Inkscape**. Afterwards, I imported the svg file and placed the sketch on the surfaces. Then, I made the face of the sketch as filled in **Data** and gave it a thickness.

[![Pattern](./images/angkukueh--pattern.jpg)](./images/angkukueh--pattern.jpg)
*>> Pattern on the surface <<*

(Unsurprisingly,) I encountered several challenges throughout this journey. Some helped me understand CAD better, for example, the first time I exported the model, the hexagonal top surface "disappeared". And thus, I realised that unlike **"Body"**, **"Surface"** does not have any thickness. To resolve this, I used **"Part"** Work Bench to add around 0.1mm **"Offset"** to the surface.

[![The 3D Model](./images/model--angkukueh.jpg)](./images/model--angkukueh.jpg)
*>> A struggling struggling journey in FreeCad <<*

However, some other challenges confused me a lot and still remain mysterious for me, even though I managed to find workarounds. For example, when I tried to use the Ang Ku Kueh (positive) model to create the negative mold using boolean. Somehow, the boolean does not treat the positive model as a whole body, instead, there is always a surface left between the lofted body and the hexagonal top face.

[![Boolean Problem](./images/model--boolean-problem.jpg)](./images/model--boolean-problem.jpg)
*>> Problem in the boolean difference when making the mold <<*

At the very desperate moment, I tried using **"Cutout"** in **"Part"** Work Bench instead of **"Boolean -> Difference"**. Unexpectedly, this worked perfectly.

[![Boolean Problem Solved (?)](./images/model--cutout.jpg)](./images/model--cutout.jpg)
*>> Problem seems solved (?) by using "Cutout" <<*

In sum, this is the modelling process:

[![Process](./images/mold--angkukueh-process.jpg)](./images/mold--angkukueh-process.jpg)
*>> Positive -> Negative -> Positive <<*

{{< rawhtml >}} 
    <script type="module" src="https://ajax.googleapis.com/ajax/libs/model-viewer/3.5.0/model-viewer.min.js"></script>
    <model-viewer alt="master mold for ang ku kueh" src="./resource/angkukueh-mastermold.glb" ar environment-image="" poster="" shadow-intensity="1" camera-controls touch-action="pan-y"></model-viewer>
{{< /rawhtml >}}

*>> The 3D model of the mastermold <<*

***
## Part 2. Mold & Cast

### 2.1 3D Printing

For 3D printing, I used **0.15mm Quality** to achieve a detailed surface as much as possible with reasonable printing time (~12 hours).

[![Slicer setting](./images/angkukueh--3d-printing.jpg)](./images/angkukueh--3d-printing.jpg)
*>> The Pursa slicer setting for 3d printing <<*

Here is the printed piece:

[![Printed piece](./images/mold--printed.jpg)](./images/mold--printed.jpg)
*>> The printed piece <<*

### 2.2 Review the safety data sheets of molding and casting materials

Different from the introduction session, I am using **[Sorta-clear 37](https://www.smooth-on.com/products/sorta-clear-37/)** to make the negative mold. Sorta-clean 37 is also produced by Smooth-on inc., it is food safe, which fits my purpose of making confectionery mold.

Similar to **Mold-star 30** which we used during the introduction session, the mix ration of the two parts follows **1A:1B by volume**. Before mixing these two parts together, the datasheet recommends us to pre-mix Part B thoroughly. When mixing A and B together, it is important to **scrape the sides and bottom of the mixing container several times**, and the mixing takes around **3 minutes**.

Unlike **Mold-star 30**, **Sorta-clear 37** is much less runny (fluid), which makes it harder to mix and easier to trap air (a.k.a, bubbles) in the liquid rubber. Thus, I moved on to the vacuum pump for degassing. The recommended pressure in the data sheet is **minimum 29inHg**, however, it seems that the vacuum pump at the lab does not really fit this. I watched the mix material **rised, broke, and fell** for 1 minute as recommended, yet, the bubbles were not eliminated at all. 😓

### 2.3 Cast, cast, cast...!

So, despite the material still contained uncountable amount of small bubbles, I followed the instruction to pour it from **a single spot aiming at the lowest point of the containment field**. 

[![mold making](./images/mold--angkukueh-silicone.jpg)](./images/mold--angkukueh-silicone.jpg)
*>> Making the negative mold <<*

Ultimately, I wanna design my own ang tuan mold and make the dessert myself one day, yet making the glutinuous rice flour skin and the red bean paste fillings would take way too much effort now for this assignment. For this round, I decided to test the mold by making jello.

(So I will make jelly for now.)

Here is a recipe I referred: **[raspberry jelly](https://www.gourmettraveller.com.au/recipe/dessert/raspberry-jelly-14125/)**. The recommended ratio from this recipe is: **250 ml liquid : 1 gelatine leaf**. In case you also have never made a jello from scrach (like me), I will summarise the process here:
1. Soak gelatine leaves in cold water,
2. Heat up the liquid (mix of juice, sugar, some fruits if needed),
3. Desolve the gelatine leaves into the hot liquid and mix thoroughly,
4. Pour the mix into the mold, put into fridge to set the jello, it takes around 4 hours - **REMEMBER, NOT THE FREEZER** - as the temperature in freezer will break the bond for the jelly, you will just get something very sandy.

#### 2.3.1 Reflections

The making of Ang Tuan & Ang Ku Kueh: hard mold (wood), flexible casting material (glutinous rice flour) -> remain still moldable after casting the pattern onto the skin.

Mold-casting happening at the lab is: flexible mold (silicone), relatively hard material after setting.

For making jelly (with complicated pattern) using this thick yet soft mold, I feel that the jelly has to be more firm than usual. For the final "succeeded" piece, I used two leaves of gelatin for less than 50ml raspberry juice. Also, I avoided puting fruits into the jelly as the fruit may break apart the jelly body during demolding.

But, but, another thing I would like to reflect on is about - "exotic object". If you also come from cultures that has not yet blended into the "norms" of the so-called "Western" lifestyle, you may have felt it always a hassle to explain some objects from your cultures. Sometimes I found myself sound like Simon Fujiwara in his performance video "Artist Book Club" when talking about things, for example, Ang Ku Kueh: *"In Hokkien, I mean the language we speak in the southern part of Fujian, which is a province in the south-eastern part of China, we call this tortoise shell cake, Ang Ku Kueh, 'Ang' means red, 'Ku' means tortoise, 'Kueh' means cake, I hope you are also aware that, this is just the romanized writing of the word..."*

The challenge is not about explaining stuff - after all, it is just about explaining unfamilar concepts to people, a totally common situation in learning and teaching. The actual difficulty lies admist the concept "exotic" itself. Strangely, it alienates the everyday object from my everyday life and transforms it into something seemingly detached from the "modern" society. As a result, the object become just another exotic - something will not be treated as serious as anything more "normal". 

In addition, these cultural traditions are often perceived as "statically" tradtional - as if it exists independently from the industrial and technological revolutions. But, hey, the confectionery is still "alive"- you can easily find it within communities and cultures influenced by the Hokkien people: Min-nan (southern Fujian) region in China, Taiwan, Singapore, Malaysia, Indonesia, or even a localised variation in Vietnam ("Bánh Quy Dừa").

That's it, and here is an interesting read: <https://docest.com/paradigm-shifts-in-the-western-view-of-exotic-arts>

***

## Global lecture

flexible, soft-pulling molding
oomoo
baby powder
- prevent bubbles
- protect the surface
cerrotru

2.5D machining

## Resrouces
Bio Art:
- Some shit (literally): [Kathy High](https://www.youaremyfuture.com/bio)