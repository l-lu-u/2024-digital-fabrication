+++
title = 'Week 07: 3D Scanning and Printing'
date = 2024-02-23T14:42:01+02:00
+++
*FDM 3D Printing & Creality Ferret 3D Scanner* 

<!--more-->

***

[![Zina's head pointcloud](./images/3dscan--zina-head-final-present.jpg)](./images/3dscan--zina-head-final-present.jpg)
*>> Pointcloud view of Zina's head (scanned by me) <<*

[![My hands pointcloud](./images/3dscan--my-hands-final-present.jpg)](./images/3dscan--my-hands-final-present.jpg)
*>> Pointcloud view of my hands (scanned by Zina) <<*

{{< rawhtml >}} 
<video width=100% controls>
    <source src="https://l-lu-u.gitlab.io/2024-digital-fabrication/videos/week07--outcome.mp4" type="video/mp4">
    Your browser does not support the video tag.  
</video>
{{< /rawhtml >}}

*>> A skeleton-like articulated monstera leaf <<*

This week's assignment consists of two parts:
1. 3D scanning with Creality Ferret
2. 3D printing (FDM)

The **group assignment** done by [Zina](https://zinaaaa.gitlab.io/digital-fabrication/index.html) can be found here: <https://zinaaaa.gitlab.io/digital-fabrication/week6_docu.html>. Zina's post covers the introduction to different printers at the lab (i.e., Ultimaker, Prusa), different test pieces, and the 3D scanning process of using Creality Ferret scanner.

***

## Part 1. 3D scanning with Creality Ferret 3D Scanner
Aalto Fablab Wiki here: <https://wiki.aalto.fi/display/AF/Creality+Ferret+3D+Scanner>

[![Creality Ferret Setup](./images/3dscan--creality-ferret-setup.jpg)](./images/3dscan--creality-ferret-setup.jpg)
*>> Creality Ferret Setup (if you want to connect to your phone) <<*

[![Creality Ferret interface on phone](./images/3dscan--creality-ferret-interface.jpg)](./images/3dscan--creality-ferret-interface.jpg)
*>> Creality Ferret Interface on phone <<*

Yet, the application always crashes once I clicked to start a new scan. Also, the interface of this app is not working well, I often can't swipe down the setting. Hence, I connected the camera directly to my laptop for the scanning. It is a bit limited by the cable, but worked fine. The desktop application will notify you to keep the optimal distance and moving speed. 

As expected, the hair can hardly be scanned and it left many hollow parts in the model. For the scan of hands, we noticed that the scanner tracked obviously better if we started the scanning from the side (as many of the classmates reflected in the review later as well).

### Some other things to do:
- I want to learn how to based on the scanned object in 3D modeling softwares
- A web component that displays interactive 3D model (recommended by [Miro](https://digital-fabrication-portfolio-miro-keimioniemi-a2f2c11a6e705b8f.gitlab.io/)): <https://modelviewer.dev/>

***

## Part 2. 3D printing (FDM)
- Software used: **Autodesk Fusion 360**
- Printer used: **Prusa MK4** (FabLab can be found here: <https://wiki.aalto.fi/pages/viewpage.action?pageId=302023189>), I have used Ultimaker printers in Väre 3D printing workshop before, so I want to learn how to work with a different type of printer this time.

For the 3D printing part, I want to learn how to design print-in-place hinges. Here is the tutorial I followed: [Learn Print-In-Place 3D Printing](https://www.youtube.com/watch?v=gjUP2e8zNKI), by [FLA Labs](https://www.youtube.com/@FLALabs).

The tutorial showcases the process of designing an articulated maple leaf, yet I found the articulated structure might work well for a structure with branches or limbs. Thus, I found a vector image of a monstera leaf: <https://www.svgrepo.com/svg/530298/leaf>. 

[![Import SVG in Fusion 360](./images/fusion--import-svg.jpg)](./images/fusion--import-svg.jpg)
*>> Import the monstera leaf SVG and then extrude it <<*

In reality, sometimes the path of the svg may not be suitable for extrusion. It happened in my case, I fixed it in Inkscape by applying **"Path -> Union"** to create a clear and enclosed outline path for the shape.
[![Fix the path in Inkscape](./images/3dprint--clean-svg.jpg)](./images/3dprint--clean-svg.jpg)

By following the tutorial, I modelled the leaf as follow:

[![The hinge](./images/fusion--hinge.jpg)](./images/fusion--hinge.jpg)
*>> The hinge <<*

[![The hinge](./images/fusion--cut-and-hinge.jpg)](./images/fusion--cut-and-hinge.jpg)
*>> I try to keep each section to be at least 16mm long so the hinge will have enough room <<*

After exporting the STL file, I used PrusaSlicer to prepare it for the printing.
[![PrusaSlicer Interface](./images/prusa--interface.jpg)](./images/prusa--interface.jpg)
*>> The interface of PrusaSlicer (after importing the STL file) <<*

I changed the infill from **"Grid"** to **"Gyroid"**, the advantages of using gyroid infill are better resistance against bending and lower weight (and thus less printing time). Some explaination can be found here: <https://3dprinting.stackexchange.com/questions/7037/what-are-the-advantages-of-gyroid-infill>.

If one wants to enable the skirt for the printing, it can be found under **"Print Settings -> Skirt"**, I changed the **"Loops (minimum)"** from "0" to "1" for my print. Though skirt does not adhere to the print, it indicates the material flow, bed leveling and the layer adhesion. By using skirt, we can identify the signs of print failure at a VERY EARLY stage and stop it to readjust the setting for the next trial.

[![Infill and skirt](./images/prusa--infill-and-skirt.jpg)](./images/prusa--infill-and-skirt.jpg)
*>> Infill and skirt <<*

[![Printing in process](./images/3dprint--articulated-leaf-process.jpg)](./images/3dprint--articulated-leaf-process.jpg)
*>> Printing in process, the print-in-place hinges <<*

The printing (without support) takes around one hour and a half. And the hinges work well (as shown in the video at the beginning).

That's it. I have some other ideas about the hinges and some questions as below:
- The print-in-place hinge seems quite suitable to make a moving rib cage decoration (SVG can be found here: <https://clipart-library.com/free/rib-cage-transparent.html>)
- Why does Prusa print a matrix of dots before printing the skirt? 