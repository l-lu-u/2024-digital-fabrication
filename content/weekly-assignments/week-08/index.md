+++
title = 'Week 08: Embedded Programming'
date = 2024-03-04T14:42:01+02:00
+++
*Embedded Programming* 

<!--more-->

***

To begin with, I have a question: "embedded programming", **WHAT is embedded within WHAT?**

With this quesion in mind, I found some explanation about the concept from this article:<https://www.coursera.org/articles/embedded-systems>. In sum, "embedded programming" refers to "embedded systems programming." They are "embedded" as theses systems are existing within a larger mechanical or electronic systems. There are four types of embeded systems:
1. **Standalone** (example: digital cameras)
2. **Network** (example: ATMs)
3. **Mobile** (example: cell phones)
4. **Real-time** (example: self-driving vehicle controls)

***

## RP2040 Datasheet reading

> "RP2040 has a dual M0+ processor cores, **DMA**, internal memory and peripheral blocks connected via **AHB/APB** bus
> fabric."


***

## Programming with XIAO RP2040

[![LED Pins](./images/embedded-prog--pins.jpg)](./images/embedded-prog--pins.jpg)

First, I observed the circuit board to find out the pins for the three surface mount LEDs:
- LED 1: D0
- LED 2: D6
- LED 3: D7

Then I tested out the codes from XIAO examples regarding the RGB LEDs, NEOPIXEL LED, and the Serial. When I modified the code to use the three surface mount LEDs as the output, I noticed that the one connected to D6 was not working. 

[![One LED is not working](./images/embedded-progamming--the-non-grounded.jpg)](./images/embedded-progamming--the-non-grounded.jpg)
*>> The LED connecting to pin D6 is not working <<*

I started to speculate if there is an open circuit because of an soldering mistake. Yet, before I set off to the mechatronic workshop (to test the circuit with a multimeter), Burak happened to come by and I was informed that this LED is not connected to the ground. 

[![Ungrounded](./images/embedded-prog--schema.jpg)](./images/embedded-prog--schema.jpg)
*>> As shown in the schematic diagram <<*

Adjusting the brightness: <https://adafruit.github.io/Adafruit_NeoPixel/html/class_adafruit___neo_pixel.html#aa05f7d37828c43fef00e6242dcf75959>

## Demo trial 1 and the code

{{< rawhtml >}} 
<video width=100% controls>
    <source src="https://l-lu-u.gitlab.io/2024-digital-fabrication/videos/week08--demo-1.mp4" type="video/mp4">
    Your browser does not support the video tag.  
</video>
{{< /rawhtml >}}

Below is the code. In this version, I store the the surface mount LEDs in an array, use 

```cpp
#include <Adafruit_NeoPixel.h>

#define BTN D1
#define NUM_PIXELS 1

int powPin = NEOPIXEL_POWER;
int neoPin = PIN_NEOPIXEL;
int boardLed = PIN_LED_G;
int ledCount = 2;
int cyclePhase[3] = {7, 14, 7};
int ledPins[2] = {D0, D7}; // surface mount LEDs (D6 is not connected to the ground)
bool ledState = LOW; // HIGH, true, and 1 mean the same
bool btnState = HIGH; // button is high as it is connected to 3.3V via a pull-up resistor
bool inCycle = LOW;
bool isWaiting = HIGH;

Adafruit_NeoPixel pixels(NUM_PIXELS, neoPin, NEO_GRB + NEO_KHZ800);

void setup() {
  pixels.begin();

  pinMode(PIN_LED_R, OUTPUT);
  pinMode(PIN_LED_G, OUTPUT);
  pinMode(PIN_LED_B, OUTPUT);
  pinMode(powPin, OUTPUT);

  for (int i = 0; i < ledCount; i++) {
    pinMode(ledPins[i], OUTPUT);
  }

  pinMode(BTN, INPUT_PULLUP);

  // Set the initial state of our LEDs
  for (int i = 0; i < ledCount; i++) {
    digitalWrite(ledPins[i], LOW);
  }

  digitalWrite(PIN_LED_R, HIGH);
  digitalWrite(PIN_LED_G, HIGH);
  digitalWrite(PIN_LED_B, HIGH);

  digitalWrite(powPin, HIGH);
  digitalWrite(boardLed, HIGH);

  Serial.begin(9600); // open the serial port at 9600 bps:
}

void loop() {

  /* A board that simulates basic menstruation*/
  // 1. Press the button to start
  // 2. boardLed (R) blink as the count
  // 3. randomly blink one of the LEDs at #7
  // 4. continue blinding the boardLed until #21
  // 5. change the pixels to dark read colour, boardLed count 7 more times
  // 6. end to the basic state, boardLed R to G, pixels fading

  bool btnReading = digitalRead(BTN);

  if (isWaiting){
    waiting();
  }

  // Check if the button state has changed
  if (btnReading != btnState) {
    if (btnReading == LOW) { // LOW means button is pressed
      Serial.println("Button pressed >>>");
      isWaiting = LOW;
      btnState = LOW;
      inCycle = HIGH;
    }
    delay(50); // debounce
    cycle();
    isWaiting = HIGH;
  } else {
      // Button released
      btnState = HIGH;
  }

}

void waiting(){
  int r = random(0,255);
  int g = random(0,255);
  int b = random(0,255);
  if (!inCycle && btnState==HIGH) {
    for (int i=0; i<50; i++){
      pixels.clear();
      pixels.setPixelColor(0, pixels.Color(r, g, b));
      pixels.setBrightness(i);
      pixels.show();
      delay(10);
    }
    for (int i=50; i>0; i--){
      pixels.clear();
      pixels.setPixelColor(0, pixels.Color(r, g, b));
      pixels.setBrightness(i);
      pixels.show();
      delay(10);
    }
  }
}

void cycle(){
  Serial.println("Entering your cycle >>>");
  boardLed = PIN_LED_R;

  int dayCount = 0;
  
  for (int i=0; i<cyclePhase[1]; i++){
    dayCount++;
    Serial.print("Day count: ");
    Serial.println(dayCount);

    digitalWrite(boardLed, HIGH);
    delay(500);
    digitalWrite(boardLed, LOW);
    delay(300);  
  }

  ovulation();

  for (int i=0; i<cyclePhase[2]; i++){
    dayCount++;
    Serial.print("Day count: ");
    Serial.println(dayCount);

    digitalWrite(boardLed, HIGH);
    delay(500);
    digitalWrite(boardLed, LOW);
    delay(300);  
  }

  bleed();
  Serial.println("End of cycle >>>");
  inCycle = LOW;
}

void ovulation(){
  Serial.println("Ovulation >>>");
  
  int ovary = random(0, 2);
  Serial.print("Ovary: ");
  Serial.println(ovary);

  for (int i=0; i<5; i++) {
    digitalWrite(ledPins[ovary], HIGH);
    delay(100);
    digitalWrite(ledPins[ovary], LOW);
    delay(100);
  }
}

void bleed(){
  Serial.println("Bleeding >>>");
  int r = random(0, 100);
  int g = 0;
  int b = 0;
  for (int i=0; i<100; i++){
    pixels.clear();
    pixels.setPixelColor(0, pixels.Color(r, g, b));
    pixels.setBrightness(i);
    pixels.show();
    delay(10);
  }
  for (int i=100; i>0; i--){
    pixels.clear();
    pixels.setPixelColor(0, pixels.Color(r, g, b));
    pixels.setBrightness(i);
    pixels.show();
    delay(10);
  }
}
```

## Working in VSCode

## Trying out MicroPython


## Final version: "Send Help!"


***
## An attempt to programme with D11C and ATiny (temporarily onhold)
XIAO is a development board

- ATiny - UPDI (connection scheme) -> found in the drawer "Programme D11C UPDI"
- D11C - SWD (connection scheme) -> found in the drawer "Programme D11C SWD"

cannot be plugged in and play -> we need **programmer** for it (<- what is that & how does it work)

> for a programmer to be used, you need to program it first by using another programmer <- how does the first programmer made? by using an Arduino (otherwise, I start to feel it like a ponzi-scheme, lol)

rename to "edbg.exe" only

1. connect the D11C to the voltage with USB cable
2. connect the programmer to the D11C and the voltage