+++
title = 'Week 06: Group Assignment - Introduction to Milling with SRM-20 & Soldering Basics'
date = 2024-02-19T13:42:01+02:00
+++
*Understanding the PCB production technology* 

<!--more-->

***

Here is a documentation of the introduction about **milling with Roland SRM-20** and **soldering using the electronics workbenches** at Aalto Fab Lab, the session was led by [Saskia](https://fabacademy.org/2023/labs/aalto/students/saskia-helinska/about.html).

🫶 *For this group documentation, special thanks to [Celeste](https://celeste_sanja.gitlab.io/digital-fabrication/) & [Mika](https://mikajarvi.gitlab.io/digital-fabrication/), who helped me take some of the screenshots and photos during the process.* 🫶

***

## Part 1. Milling with Roland SRM-20

At Aalto Fab Lab, there are two machines that can be used for PCB milling: 1). Roland SRM-20, and 2). Roland Modela MDX-40. Currently at the lab, SRM-20 is mainly used for PCB milling while MDX-40 can be used for 3D milling as well.

To use **Roland SRM-20**, you can always refer to the Aalto Fab Lab Wiki about the process: <https://wiki.aalto.fi/display/AF/PCB+Milling+with+Roland+SRM-20>

The existing PCB design used for this week's milling can be found here: [Tarantino](https://gitlab.fabcloud.org/pub/programmers/tarantino), under the **/Gerber** folder:
- Tarantino-B_Cu.gbr *// the back*
- Tarantino-F_Cu.gbr *// the front*
- Tarantino-PTH-drl.gbr *// parts needed to be drilled*

For the milling, there are approximately 8 steps:
1. Prepare the copper clad board for the machine,
2. Prepare the circuit board paths (in **Gerber** format) in software **CopperCAM**,
3. Set up the the machine using **VPanel** (the controller of SRM-20) and install the milling bit,
4. Calibration: Set the **Z-origin** for the bit,
5. Calibration: Set the **X/Y-origin** for the bit,
6. Import the milling file in VPanel,
7. Change the tool for a different type of operation,
8. Done & clean-up <3

Below are the detailed process of doing such :)

***

### Step 1. Preparation: Milling with the "Orange Monster" at Aalto Fablab
**SRM-20** is the orange colour boxy machine at the corner of the lab.
[![The Workstation](./images/milling--srm20--workstation.jpg)](./images/milling--srm20--workstation.jpg)
*>> A glance of the workstation and most tools needed <<*

Before placing the copper clad board to the machine's working platform, use the metal sponge from the first layer of the drawer to romove the wax layer. 

In the same drawer, you can find most of the tools you need for milling with SRM-20:
- **double-sided tape:** to fix the board on the working platform,
- **brush:** to dust off the copper powder,
- **cutter & knife:** to trim the tape, or detach the board from the platform
- **different bits:** to engrave or cut,
- **vernier scale:** to measure the material thickness.

Then, apply the double-sided tape to the back of the board so that the board will stay fixed on the working platform during the milling. Afterwards, place it at the south-west (left-bottom) corner - the origin point of the machine setting. 

Use the brush or vacuum to clean the working platform, so as to ensure no left-over from the last work.
[![Preparing the board](./images/milling--prep-the-clad.jpg)](./images/milling--prep-the-clad.jpg)
*>> Saskia is preparing the copper clad board <<*

When taping the board, avoid creating bubbles or creases which will create a height difference on the surface and thus affects the milling. As the copper boards may be slightly warped (like this time), cutting the board into smaller pieces may also help to create a flat surface for milling.

### Step 2. Tool Paths: Prepare the Gerber File in CopperCAM
Here, the **"tool"** refers to the different type of cutters/bits we will be using for the milling. We will change the tool based on the type of operation, for example, we are using **Tool 1 (60 degree bit)** for the engraving operation and **Tool 3 (1mm cutter)** for the drilling and cutting job.

To specify all these setting, we will be using this software: **[CopperCAM](https://www.galaad.net/coppercam-eng.html)**. Regarding the creation of tool paths, you may refer to the Wiki page here: <https://wiki.aalto.fi/display/AF/Creating+Tool+Paths+with+CopperCAM>

1. Firstly, go to **Parameters** and uncheck **Tracks -> pads auto.converting**, 
2. Go to **File > Open > New circuit** to open the first layer of the board, in this case, should be the **"Tarantino-F_Cu.gbr"** file (and we are producing a single-sided board this week), 
3. A message popped up asking if the highlighted line is the contour path of your board, don't panic, click **Yes**, [![Open Gerber File in CopperCAM](./images/milling--open-file-in-coppercam.jpg)](./images/milling--open-file-in-coppercam.jpg)
4. Right click on the text elements (i.e.,**"TARANRINO..."**), choose **"Engrave tracks as centerline"** from the menu and then click **Yes** on the popup, now the engraved text will be treated differently from the circuit contour [![Text element](./images/milling--text.jpg)](./images/milling--text.jpg)
5. User **File > Open > Drill** to open the drill file, in this case, should be the **"Tarantino-PTH-drl.gbr"** file,
    1. For other cases, choose **File > Open > Additional layer** option, you may add up to 4 additional layers here in CopperCAM
    2. When adding new layer, if elements (e.g., drilling holes) are misaligned, use the **Reference Pad** feature to adjust and align them, the picture below is a simple demo (a more comprehensive guide can be found in this [tutorial by Kris](https://youtu.be/TvymRjyVZyQ?si=K9koxTI_P67TldrT&t=180)):[![Reference Pad](./images/milling--reference-pad.jpg)](./images/milling--reference-pad.jpg)
6. To select the origin, go to **File > Origin**, set to **"X:0, Y:0"**,
7. Go to **Parameters > Selected tools** to activate/confirm tools for each seperate operation; for this week's assignment, we are using **Tool 1 for Engraving, Tool 3 for Hatching and Cutting;** other parameters as such: Engraving & Hatching Depth - 0.1mm, Cutting Depth - 1.8mm, Drilling Depth - 2.4mm, Engraving/Hatching/Cutting Speed - 8mm/s, Boring Speed - 4mm/s [![Selected Tools](./images/milling--selected-tools.jpg)](./images/milling--selected-tools.jpg)
8. Click on **Calculate the contour**, for the ease of soldering (especially for beginners), set **Number of successive controus** to 4, and **Extra contours around pads** to 2 [![Set contours](./images/milling--calculate-contours.jpg)](./images/milling--calculate-contours.jpg)
9. Click on **File > Dimensions** to check on the board dimensions, once click "OK", there will be a popup asking if we want to modify the existing values, as the dimensions have been set according to the bits used for this week's assignment, click **"No"!!!** [![Check dimensions](./images/milling--dimensions.jpg)](./images/milling--dimensions.jpg)
10. Click on **Mills** to check the work order, which should be "Engrave -> Drill -> Cut", finally, click **OK** and save the **.nc** file, which will be seperated according to the tools used [![Save for milling](./images/milling--mills.jpg)](./images/milling--mills.jpg)

### Step 3. VPanel & Install the Milling Bit

**!!!Huom/Attention!!!** Be gentle and cautious when handling the bits, the tip is very fragile and will be damaged once it drops - and, they are quite not cheap </3

1. Open **VPanel for SRM-20** software on the computer, which is used to control the Roland milling machine,
2. Make sure that both the **"Machine Coordinate System"** and **"Set Origin Point"** are set to **G54**, [![VPanel Interface](./images/vpanel--interface.jpg)](./images/vpanel--interface.jpg)
3. Check the **Spindle Speed, which should be one step before the maximum**.
4. Click on **Setup** to check that "Command Set:" **NC Code**, "Unit:" **Millimeters**, "Y-axis using keypad:" **Move cutting to the desired location**, and then click **"OK"**. 
[![VPanel Setup](./images/vpanel--setup.jpg)](./images/vpanel--setup.jpg) *>> Image credit: Mika <<*
5. Use the arrow keys on the interface to move the tool holder to the middle so it's easier to install the bit, as the work will start from **engraving**, we should install the **Tool 1 (60 degree)** first,
[![Choose the bit](./images/milling--bit.jpg)](./images/milling--bit.jpg)
6. Take the **hex key** (attached to the machine by a magnet) to gently fix the bit to the tool holder (at this stage, it is better to slightly push the bit up and let the stopping ring be contact with the holder) 
[![Install the bit](./images/milling--install-bit.jpg)](./images/milling--install-bit.jpg) *>> Image credit: Celeste<<*

### Step 4. Calibration: Set the Z-origin
For Step 4 & 5, we are "telling" the machine where it should refer as the "0" - the origin point and the surface level for precise cutting. We will start the zeroing process from the **Z-axis** first (up/down) and then move to **X/Y** (left/right & back/forth).

When the bit gets closer to the surface, change the **Cursor Step** from **Continue** to **x100** or **x10**, which moves less steps at one go but we can thus avoid the bit tip accidentally hits the surface.

**!!!Huom/Attention!!!** If nothing moves, close the machine cover :)

Here are the steps to set the Z-origin:

1. Use the arrow button to lower the bit closer to the surface until there is about 3~5mm distance between the tip and surface,
2. Use the hex key to release the bit (while the other hand should still hold the bit avoid it dropping down abruptly), move the bit down till it touches the material's surface, and then tighten it again,
3. Press **Set Origin Point > "Z" button** to set Z-origin, click **"Yes"** to the pop-up message, the value of Z now should be "0.00mm",
4. Now, **move up the bit** slightly using the arrow button, so as to avoid the tip spindling across the surface.
[![Set Z-origin](./images/milling--z-origin.jpg)](./images/milling--z-origin.jpg) 

### Step 5. Calibration: Set the X/Y-origin
1. Make sure the bit is not touching the material now,
2. Use arrow buttons to move the bit to the starting point -> **Remember the bit will start from the south-west (bottom-left) corner**
3. Press **Set Origin Point > "X/Y" button** to set X/Y-origin, the value of X and Y now both "0.00mm",

### Step 6. Import the working file
1. Click **Cut > Delete All > Add** to import the working file,
2. Select the file accordingly, in this case, we start with the **CopperCAM-F-T1.nc** for engraving first (T1 refers to Tool 1), and later the **CooperCAM-F-T3.nc** for the drilling and cutting job (T3 refers to Tool 3), 
3. Once you click **Output**, the machine will start.

[![Import working file](./images/milling--import-file.jpg)](./images/milling--import-file.jpg) 

### Step 7. Change the tool (a.k.a. a different bit)
After finishing the engraving, we need to change the tool to do the drilling and cutting operations.
1. Press **View** to move the working platform closer to you (south-west corner), use the vacuum or brush to dust off the copper powders,
2. Replace the bit to **Tool 3: 1.00** for drilling and cutting,
3. Repeat **Step 4. Set the Z-origin** - **BUT, don't reset the X/Y origin!!**
4. Repeat **Step 6. Import the working file** to start the job.

### Step 8. Done! (if nothing happens) & Clean up

If everything works as expected, we will have our board. Here are the steps of removing your board and cleanning up the workstation:
1. Carefully use the butterknife-like knife to take off the clad and the cut-out board,
2. Remove the tape on the backside and the working platform (so it will stay flat and neat for others), if the left-over copper clad board is big enough, you should put it into the **Reusable PCB Stock** box, if not, kindly put it into the **Electronic Waste** bin behind you.
3. Use the vacuum to clean the dust inside the machine,
4. Ensure the hex key is placed back to the magnet,
5. Put all the tools back to the drawer.

[![Reusable PCB Stock box](./images/milling--reusable-fcb-stock.jpg)](./images/milling--reusable-fcb-stock.jpg) 

That's it :D

Yet, in reality, THINGS DO HAPPEN. For example, in the trial done in the introduction session, we forgot to check the **Dimensions** for the "Drills" work in CopperCam before sending it to **Mills**, the holes are misaligned from the engraved circuit. 

[![Misaligned Drilled Holes](./images/milling--misalignment.jpg)](./images/milling--misalignment.jpg) 
*>> Misaligned drilled holes <<*

Also, we did not press the bit to be **slightly more in contact** with the board when lowering it to the Z-origin, the end results left some copper within the contour - which can be fixed later through manual work. 

I also encountered some unexpected challenges, I will elaborate in my individual assignment post: <https://l-lu-u.gitlab.io/2024-digital-fabrication/weekly-assignments/week-06--individual/>

***

## Part 2. Electronics soldering workbench

The introduction to electronics soldering workbench consits of 3 topics:
1. Clean the PCB board,
2. Insert the rivets to the drilled holes,
3. Soldering basics.

### 1. Clean the board for soldering

After removing the carved, drilled, and cut circuit board, you may use the alcohol to clean the surface (remember to wear gloves).
[![Clearning the PCB board](./images/soldering--cleaning-board.jpg)](./images/soldering--cleaning-board.jpg) 

### 2. Insert the rivets
To start with, find the tools needed for this task:
- A pair of rivet punch tools, one to stretch the rivet and one to flatten it,
- The 1MM rivets,
- A hammer.

[![Tools for rivets insertion](./images/soldering--insert-rivets.jpg)](./images/soldering--insert-rivets.jpg) 

The PCB rivets we are working with are (tiny) hollow tube with a flat ring at one end. Now, install the rivet through one of the drilled holes, turn to the other side without the rivet falling out, and then place the board flat on the small metal platform clamped at the table side.

First, use the **stretch tool**, hold it against the tubular end of the rivet, and gently hit it about twice.

Then, use the **flatten tool**, hold it against the stretched rivet end, and gently hit it about twice (and check if it fits neatly, if not, hit it one more time I guess).

By using rivets, we can connect both sides of the board electrically without an extra via. Yet, **we will still need to solder to these rivets to ensure a good electrical connections.**

[![Stretch and Flatten](./images/soldering--stretch-and-flatten.jpg)](./images/soldering--stretch-and-flatten.jpg) 
*>> Saskia demonstrating how to install the rivets <<*

### 3. Soldering basics
[![Soldering station](./images/soldering--soldering-station.jpg)](./images/soldering--soldering-station.jpg) 
1. Turn on the **fume extractor** and test if it is working,
2. Turn on the **soldering station**, for this board, set the expected iron temperature about 320~340°C,
3. Use the **flux pen** to apply some flux at the positions where you solder, and then **START SOLDERING**:
    1. One tip is to apply some tin at one side that you will place your component, then use a **tweezer** to hold the component tightly and stably against the board, and now when you move the soldering iron to the spot, the pre-applied tin will melt - now, **move away the iron without releasing the tweezer**, let the tin to cool down, and now the component will stay at the place for you to complete soldering the other end;[![Soldering tips](./images/soldering--soldering-tips.jpg)](./images/soldering--soldering-tips.jpg) 
    2. For components with conductive ends along two sides, like the headers for the mircocontroller boards, after fixing one end, solder the other end **diagonally;**
    2. Note that, some components have **polarity**, for example, the LEDs we are using. Examine the LED closely, you will see **green stripes on one end**, which denotes the **cathode side**, oriente the component according to the schematic.
4. Use a **multimeter** to test if parts are properly connected, or if there is any short circuit.
5. After you finish, **REMEMBER TO TURN OFF THE SOLDER IRON** and the fume extractor :)

***

That's it, hope you find this post helpful <3