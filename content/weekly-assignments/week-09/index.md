+++
title = 'Week 09: Computer-Controlled Machining'
date = 2024-03-07T14:42:01+02:00
+++
*snug-fit & CNC* 

<!--more-->

***

[![CNC project: A Tea Table](./images/cnc--final.jpg)](./images/cnc--final.jpg)
*>> Final: A Tea Table <<*

This post (probably) consists of two parts: 
1. A documentation about the safety introduction details and general flow of using the machine;
2. The process of making **(design+mill+assemble)** something big (**a tea tray for my final project**);

***

joints

## Part 1. Safety Introduction & General Flow

**Recontech 1312** - the most dangerous murderbot, no, machine at the lab.

### 1.1 General about the machine setup
This machine is used mainly for wood & soft materials like foam. Two other important part of the machine:
- a vacuum table
- the machine controller at the left, connected to a computer (through an ethernet cable)

### 1.2 Tools

"Tool" refers to the CNC router bit.

[![Tools](./images/recontech--tools.jpg)](./images/recontech--tools.jpg)
*>> Drill & Milling tools <<*

-  Type of tools
    - **Drilling tool**: can go only vertically, goot at pulling chips out and drilling holes *(cannot be used for milling!!!)*;
    - **Metal drilling tool**: with a pointy tip, for metal - but we are not doing metal drilling here (at Aalto FabLab) as the air chip extraction system here is designed to accept wooden chips only;
    - **Two-flute flat end milling tool**: can go sideways - milling tools are designed to pick up material on the sides, this one can be used for cutting and engraving - *but not good at going downwards,* if we let it go downards, it is better to do it in a circular motion (as it is good at PICKING MATERIAL ON SIDES);

### 1.3 Handling the material
*WEAR GLOVES* -> as the material (probably) chips
Measure the material for tool path generation:
- width (mm)
- length (mm)
- thickness (mm) -> soft material is likely to shrink or bend overtime

Tools are inside the green drawer. The first digit of the tool numbering denotes the diameter of the flute. On the wall, there's a table with more information about each tool, such as the flute diameter, flute length, number of flutes (usually 2 in the drawer), shank diameter (for mounting the tool).

It is important to *MEASURE THE TOOL* before setting up the job, because sometimes things may not be put inside the correct boxes (oh no!).

**Downcut tool & Upcut tool** -> briefly mentioned, it sounds that 


### 1.4 Job setup
*DON'T WEAR GLOVES* -> as the fabric may get tangled

- Measure the thickness of the material
- X/Y Datum postition
- Create a rectangle **(?)**
- Tool measurement **(?)**
- Generate tool paths (ho! just like the PCB milling) **(?)**
- Fill edge tool
- T-bone fillet
- Generate the toolpaths **(?)**
- Cut depth
- Feeds & Speeds
- Spindle speed
- Calculate the feed rate
- Explore **(?)** the pocket toolpath
- Export the toolpaths

### 1.5 Work with the machine
1. Turn on the machine
2. Insert the tool to tool holder
3. Set the origin
4. Activate the vacuum pump
5. Remove motion
6. Turn off the machine

### 1.6 Post-processing
1. General
2. Sanding


### VCarve
T-bone & Dog-bone Fillet

Auto-nesting

Set the machine vectors to remove the material from the outside
!!! Add tabs to toolpath (so the piece won't detach and hit around during the milling)

### CNC machine
cutting parameter table
- spindle speed
- RPM
- chip loadd
- number of flutes

## Part 2. The process of making

**Material & Parameters**
- Material: soft plywood sheet
- Dimension: 120x120 (cm) -> 1200x1200 (mm)
- Thickness: 15(mm)

### 2.1 Designing in FreeCAD
[![Sketch](./images/cnc--sketch.jpg)](./images/cnc--sketch.jpg)
*>> Sketch of the tea tray <<*

I came across some peranakan lacquer ware design and a backgammon board from Tang dynasty, also an interestingly interlocking chair leg structure. Hence, I changed my design for the tea tray from a rectangular shape to a regular octagon.

[![Inspiration](./images/cnc--inspirations.jpg)](./images/cnc--inspirations.jpg)
*>> My inspirations <<*

In general, the working process in FreeCAD can be described as below:
*Spreadsheet* -> *Part Design* -> *A2 Plus (Assembly)* -> *Path*

[![Modeling in FreeCAD](./images/cnc--overview-freecad.jpg)](./images/cnc--overview-freecad.jpg)
*>> The model in FreeCAD <<*

(Note from others' sharing in class: Make the tab bigger and steadier as the plywood will easily chip)

[![Nesting & Path in FreeCAD](./images/cnc--nest-path.jpg)](./images/cnc--nest-path.jpg)
*>> I tried the nesting using A2Plus in FreeCAD (but then I stopped at the Path part) <<*

As shown in my initial sketch, I originally planned to carve out vaginal pattern on the tray (to let liquid leak down). In fact, I did have some attempts in Inkscape for the pattern design. Yet, somehow the pattern did not match the octagon that well. Also, I was concerned about the carved out section might chip a lot due to the quality of the plywood.

#### Fusion 360 vs FreeCAD
You don't have to modify the design in FreeCAD for the t-bone.

#### Joinery / 榫卯（Sunmao）
Sources:
1. A Guide to CNC Wood Joinery: <https://info.lagunatools.com/solid-wood-joinery-using-a-cnc-machine>
2. CNC Basecamp - Episode 002. Joinery with a CNC: <https://www.woodsmith.com/article/episode-002-joinery-with-a-cnc/>

Consideration - strength & look
- Box joint
- Catch tenon


### 2.2 Millinng

The unexpected difficulty happened after I import the **.DXF** file to VCarve, there are many overlapping lines in the vector.
[![VCarve Overlaps](./images/cnc--vcarve-overlaps.jpg)](./images/cnc--vcarve-overlaps.jpg)
*>> Overlaps in VCarve <<*

**!!!Note**
To avoid breaking the tool, one should:
- choose right depth of cut (1/2 diameter of tool is safe)
- calculate the feed rate (FR = SS * CL * NF) accordingly 

### 2.3 Assembling

After removing the tabs, I learnt how to use the wood router to make the edge cleaner.

[![Wood router](./images/cnc--wood-router.jpg)](./images/cnc--wood-router.jpg)
*>> Demonstration of how to use the wood router <<*

[![The leg structure](./images/cnc--legs.jpg)](./images/cnc--legs.jpg)
*>> Leg Assembly <<*

[![With tray on](./images/cnc--tray.jpg)](./images/cnc--tray.jpg)
*>> With the tray on, the grain texture also looks vibey (like some cloud patterns) <<*

***
## Resources
- Inspiration: https://teenage.engineering/
- Joinery