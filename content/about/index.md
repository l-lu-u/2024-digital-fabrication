+++
title = "About"
description = "lù, and some stuff done (until 2024)"
date = "2024-01-12"
aliases = ["about-us","about-lu","contact"]
author = "Lu"
+++

lù, and some stuff done (until 2024)

```md
+-----------------+
|  . . . . . .    |
| .           .   |
|+ L U . U +      |
| .           .   |
|  . . . . . .    |
|                 |
|    *    *    *  |
|   * *  * *  * * |
|    *    *    *  |
+-----------------+
```

***

## Lu / 璐 (Pinyin: Lù; “fine jade”)

[![Hands of Lu](./images/self-hands.jpg)](./images/self-hands.jpg)

**Lù (CN; SG)**, designer and media artist based in Helsinki （[Portfolio](https://l-lu-u.github.io/)）.

Born in Fuzhou, China, living in Singapore before moving to Finland - from the tropics 🌴 to the Nordics 🌲, I was growing up with interests in finding connections across boundaries, between seemingly contrasting things.

In my practice, I work with data and then translate the statistical and textual information into other forms - audiovisual, physical, and/or tactile.

My research interests focus on the ability of media and (collaborative) design in alternative storytelling and thus explore possible futures. 

I'm open for jobs in service design, design research, and any initiatives aligning with my practices and interests. If you'd like to get in touch, feel free to contact me through email 📥 <chenlu.ciel@gmail.com> or 🤝 [LinkedIn](https://www.linkedin.com/in/lu-chen-43177191/).