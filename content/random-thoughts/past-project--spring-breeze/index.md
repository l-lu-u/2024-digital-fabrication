+++
image = "past-project--spring-breeze.jpg"
date = "2020-10-14"
title = "Spring Breeze, Autumn 2020 (past project for further iterations)"
type = "gallery"
+++

**Spring Breeze / 望春风** is a 3D printed lamp whose form is generated based on my mood flunctuations throughout the 7-month application period to Aalto University.