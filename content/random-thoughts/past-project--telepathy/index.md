+++
image = "past-project--telepathy.jpg"
date = "2021-03-08"
title = "Telepathy / 以心电信, Spring 2021 (past project for further iterations)"
type = "gallery"
+++

**Telepathy / 以心电信** is an interactive audiovisual public installation.

{{<youtube id="ZQPupGtUbOo" class="video">}}