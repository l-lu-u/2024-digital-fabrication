+++
image = "past-project--spill.jpg"
date = "2023-12-14"
title = "SPILL - Autumn 2023, Version 1 (past project for further iterations)"
type = "gallery"
+++

**SPILL** is a kinetic installation that represents menstrual cycles mechanically with a set of tea wares.

{{<youtube id="7HFCcQQNFeQ" class="video">}}

Link to detailed documentation: <https://newmedia.dog/students/2023/lu-chen/physical-computing/final-project/>

Inspired by the Finnish folklore “harakointi” - the act of revealing a woman’s genitals to a target as supernatural magic to protect or curse, SPILL intends to spill the tea on a vaginal topic: menstruation. This kinetic installation demonstrates simplified menstrual cycles using tea wares and a tampon. The mechanism is controlled by anonymous data on menstrual cycle lengths from a healthcare research project in 2012.

Menstruation is the regular discharge of blood and mucosal tissue from the inner lining of the uterus through the vagina. Despite being a common (and significant) biological process, it is often a taboo topic. Stigmas and myths surrounding menstruation remains even until nowadays. For example, advertisement of period products uses blue liquid to represent blood, many girls and women are not allowed to be in the kitchen or attend ritual practices during their menstruation, and some languages adopt “euphemisms” for menstruation.

With this work, let’s spill some tea, blood, or truth about menstrual cycles. *sips tea*