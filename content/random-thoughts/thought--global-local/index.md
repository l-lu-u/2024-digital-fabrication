+++
image = "past-project--telepathy.jpg"
date = "2024-02-21"
title = "globalisation"
type = "gallery"
draft = true
+++

Global lecture:
- Word usage - "to solder" and "to weld" - it means the same thing in some languages (e.g., Spanish, Arabic, Portuguese, Chinese), even though there are two seperate terms for it in English

Local situation:
- What if there is a local transportation strike happening
